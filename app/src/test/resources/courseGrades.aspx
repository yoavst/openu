

<!--
  ~     This file is part of OpenU.
  ~
  ~     OpenU is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU General Public License as published by
  ~     the Free Software Foundation, either version 3 of the License, or
  ~     (at your option) any later version.
  ~
  ~     OpenU is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU General Public License for more details.
  ~
  ~     You should have received a copy of the GNU General Public License
  ~     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
  ~
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
    Course grades
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/CourseGrades.css" type="text/css" /></head>
<body>
<form name="frmMain" method="post" action="courseGrades.aspx?id=04101&amp;semester=2015%u05d1" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNjYxNTczMTgwD2QWAgIBD2QWAgIFDxYCHgtfIUl0ZW1Db3VudAIKZGQ6mT9DC280lvjBEhEveQwUT0N3T013e3ZGHdnQT/dGTA==" />

    <div class="gradientBG coursesGradesWrap">
        <div id="finalGrade">
            <!-- replace static texts with texts from .resx or with a protected variable/property from the page-->
            <h3>
                ציון סופי בקורס:<span>90</span>
            </h3>
        </div>
        <div class="courseGradeGreyRow">
            <span class="clm1">&nbsp</span> <span class="clm2">סוג</span> <span class="clm3">ציון</span>
            <span class="clm4">תאריך</span>
        </div>

        <ul>

            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=1" style="text-decoration:none;"><li><span class="clm1">1</span><span class="clm2">ממ"ח</span><span class="clm3">92</span><span class="clm4">15.04.15</span><img src="images/gray_arrow.png" alt="1" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=2" style="text-decoration:none;"><li><span class="clm1">2</span><span class="clm2">ממ"ח</span><span class="clm3">96</span><span class="clm4">27.04.15</span><img src="images/gray_arrow.png" alt="2" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=3" style="text-decoration:none;"><li><span class="clm1">3</span><span class="clm2">ממ"ח</span><span class="clm3">92</span><span class="clm4">17.05.15</span><img src="images/gray_arrow.png" alt="3" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=4" style="text-decoration:none;"><li><span class="clm1">4</span><span class="clm2">ממ"ח</span><span class="clm3">83</span><span class="clm4">24.06.15</span><img src="images/gray_arrow.png" alt="4" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=5" style="text-decoration:none;"><li><span class="clm1">5</span><span class="clm2">ממ"ח</span><span class="clm3">67</span><span class="clm4">05.07.15</span><img src="images/gray_arrow.png" alt="5" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=11" style="text-decoration:none;"><li><span class="clm1">11</span><span class="clm2">ממ"ן</span><span class="clm3">91</span><span class="clm4">13.04.15</span><img src="images/gray_arrow.png" alt="11" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=12" style="text-decoration:none;"><li><span class="clm1">12</span><span class="clm2">ממ"ן</span><span class="clm3">81</span><span class="clm4">29.04.15</span><img src="images/gray_arrow.png" alt="12" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=13" style="text-decoration:none;"><li><span class="clm1">13</span><span class="clm2">ממ"ן</span><span class="clm3">95</span><span class="clm4">15.05.15</span><img src="images/gray_arrow.png" alt="13" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=14" style="text-decoration:none;"><li><span class="clm1">14</span><span class="clm2">ממ"ן</span><span class="clm3">86</span><span class="clm4">31.05.15</span><img src="images/gray_arrow.png" alt="14" class="ArrowLeftGrey" /></li></a>
            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=04101&semester=2015%d7%91&gid=82" style="text-decoration:none;"><li><span class="clm1">82</span><span class="clm2">בחינת גמר</span><span class="clm3">90</span><span class="clm4">09.07.15</span><img src="images/gray_arrow.png" alt="82" class="ArrowLeftGrey" /></li></a>
        </ul>
    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
