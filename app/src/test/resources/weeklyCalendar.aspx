

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
    Weekly Event Table
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/WeeklyCalendar.css" type="text/css" /></head>
<body>
<form name="frmMain" method="post" action="WeeklyCalendar.aspx?date=30%2f08%2f2015" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE0NDUyNTk0MTIPZBYCAgEPZBYGAgEPFgIeBGhyZWYFV253dHAuaW52aWV3Oi8vc2hlaWx0YS5hcHBzLm9wZW51LmFjLmlsL29wbW9iaWxlL1dlZWtseUNhbGVuZGFyLmFzcHg/ZGF0ZT0wNiUyZjA5JTJmMjAxNWQCBQ8WAh8ABVdud3RwLmludmlldzovL3NoZWlsdGEuYXBwcy5vcGVudS5hYy5pbC9vcG1vYmlsZS9XZWVrbHlDYWxlbmRhci5hc3B4P2RhdGU9MjMlMmYwOCUyZjIwMTVkAgkPFgIeC18hSXRlbUNvdW50AgQWCGYPZBYCAgMPFgIfAQIBZAIBD2QWAgIDDxYCHwECAWQCAg9kFgICAw8WAh8BAgJkAgMPZBYCAgMPFgIfAQIBZGTgGDE+ucUFnsTWS0a11xmM5jT8PtkbpEoyLXk5dcsUsQ==" />

    <div class="gradientBG weeklyTblWrap">
        <a href="nwtp.inview://sheilta.apps.openu.ac.il/opmobile/WeeklyCalendar.aspx?date=06%2f09%2f2015" id="weeklyLeftRef" class="weeklyLeft">
        </a>
        <h2>
            <span>אירועים בשבוע 36</span><div>(30.08.15-05.09.15)</div>
        </h2>
        <a href="nwtp.inview://sheilta.apps.openu.ac.il/opmobile/WeeklyCalendar.aspx?date=23%2f08%2f2015" id="weeklyRightRef" class="weeklyRight">
        </a>


        <div class="weekDayRow">
            <span class="day">יום ראשון</span><span class="dayDate">30.08.15</span>
        </div>
        <ul class="weekDayEvents">

            <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/EventDetails.aspx?eid=505101880&date=30%2f08%2f2015"><img src="images/telephone_icon.png" class="icon" alt="telephone icon" /><div class="eventInterval"><span>&nbsp;</span></div><div class="eventDetails"><span class="eventInfo">ממ"ן אלגברה לינארית 1</span><span class="eventLocation"></span></div><img src="images/gray_arrow.png" /></a></li>
        </ul>

        <div class="weekDayRow">
            <span class="day">יום שלישי</span><span class="dayDate">01.09.15</span>
        </div>
        <ul class="weekDayEvents">

            <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/EventDetails.aspx?eid=-753033372&date=01%2f09%2f2015"><img src="images/telephone_icon.png" class="icon" alt="telephone icon" /><div class="eventInterval"><span>09:00 - 11:00</span></div><div class="eventDetails"><span class="eventInfo">מפגש אלגברה לינארית 1</span><span class="eventLocation">מרכז לימוד תל אביב - קמפוס רמת אביב 630 בקבוצה 10</span></div><img src="images/gray_arrow.png" alt="מרכז לימוד תל אביב - קמפוס רמת אביב 630 בקבוצה 10" /></a></li>
        </ul>

        <div class="weekDayRow">
            <span class="day">יום רביעי</span><span class="dayDate">02.09.15</span>
        </div>
        <ul class="weekDayEvents">

            <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/EventDetails.aspx?eid=-788908782&date=02%2f09%2f2015"><img src="images/telephone_icon.png" class="icon" alt="telephone icon" /><div class="eventInterval"><span>&nbsp;</span></div><div class="eventDetails"><span class="eventInfo">ממ"ח אלגברה לינארית 1</span><span class="eventLocation"></span></div><img src="images/gray_arrow.png" /></a></li>
            <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/EventDetails.aspx?eid=2101648201&date=02%2f09%2f2015"><img src="images/telephone_icon.png" class="icon" alt="telephone icon" /><div class="eventInterval"><span>21:00 - 22:00</span></div><div class="eventDetails"><span class="eventInfo">הנחיה טלפונית אלגברה לינארית 1</span><span class="eventLocation"></span></div><img src="images/gray_arrow.png" /></a></li>
        </ul>

        <div class="weekDayRow">
            <span class="day">יום חמישי</span><span class="dayDate">03.09.15</span>
        </div>
        <ul class="weekDayEvents">

            <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/EventDetails.aspx?eid=-888692892&date=03%2f09%2f2015"><img src="images/telephone_icon.png" class="icon" alt="telephone icon" /><div class="eventInterval"><span>09:00 - 11:00</span></div><div class="eventDetails"><span class="eventInfo">מפגש אלגברה לינארית 1</span><span class="eventLocation">מרכז לימוד תל אביב - קמפוס רמת אביב 630 בקבוצה 10</span></div><img src="images/gray_arrow.png" alt="מרכז לימוד תל אביב - קמפוס רמת אביב 630 בקבוצה 10" /></a></li>
        </ul>

    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
