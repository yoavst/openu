

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
    Weekly Event Table
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/WeeklyCalendar.css" type="text/css" /></head>
<body>
<form name="frmMain" method="post" action="WeeklyCalendar.aspx?date=20%2f09%2f2015" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE0NDUyNTk0MTIPZBYCAgEPZBYGAgEPFgIeBGhyZWYFV253dHAuaW52aWV3Oi8vc2hlaWx0YS5hcHBzLm9wZW51LmFjLmlsL29wbW9iaWxlL1dlZWtseUNhbGVuZGFyLmFzcHg/ZGF0ZT0yNyUyZjA5JTJmMjAxNWQCBQ8WAh8ABVdud3RwLmludmlldzovL3NoZWlsdGEuYXBwcy5vcGVudS5hYy5pbC9vcG1vYmlsZS9XZWVrbHlDYWxlbmRhci5hc3B4P2RhdGU9MTMlMmYwOSUyZjIwMTVkAgcPFgIeB1Zpc2libGVnZGSeywr0u5Dvj/5Hbi4xaPmV97e8sz75pzRfojW9yszX2Q==" />

    <div class="gradientBG weeklyTblWrap">
        <a href="nwtp.inview://sheilta.apps.openu.ac.il/opmobile/WeeklyCalendar.aspx?date=27%2f09%2f2015" id="weeklyLeftRef" class="weeklyLeft">
        </a>
        <h2>
            <span>אירועים בשבוע 39</span><div>(20.09.15-26.09.15)</div>
        </h2>
        <a href="nwtp.inview://sheilta.apps.openu.ac.il/opmobile/WeeklyCalendar.aspx?date=13%2f09%2f2015" id="weeklyRightRef" class="weeklyRight">
        </a>
        <div id="NoEvents" class="NoEvents">
            אין אירועים בטווח התאריכים</div>

    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>