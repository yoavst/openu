

<!--
  ~     This file is part of OpenU.
  ~
  ~     OpenU is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU General Public License as published by
  ~     the Free Software Foundation, either version 3 of the License, or
  ~     (at your option) any later version.
  ~
  ~     OpenU is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU General Public License for more details.
  ~
  ~     You should have received a copy of the GNU General Public License
  ~     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
  ~
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
    Course meetings
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/CourseMeetings.css" type="text/css" /></head>
<body>
<form name="frmMain" method="post" action="courseMeetings.aspx?id=20109&amp;semester=2015%u05d2" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE0NjgwMzkyNjAPZBYCAgEPZBYGAgEPFgIeBGhyZWYFYW53dHAucHVzaDovL3NoZWlsdGEuYXBwcy5vcGVudS5hYy5pbC9vcG1vYmlsZS9Db3Vyc2VNZWV0aW5nc0RldGFpbHMuYXNweD9jSUQ9MjAxMDkmc2VtPTIwMTUlZDclOTIWBgIBDxYCHglpbm5lcmh0bWwFAjEwZAIDDxYCHwEFF9eU16DXl9eZ15Qg157XldeS15HXqNeqZAIFDxYCHwEFLCDXmdeV150g15IgMTE6MDAtMDk6MDAg15nXldedINeUIDExOjAwLTA5OjAwZAIDDxYCHwEFlQE8YSBocmVmPSJud3RwLm1hcDovL21hcHMuZ29vZ2xlLmNvbS9tYXBzP3E916fXnNeQ15XXlteg16ggMTYsINeo157XqiDXkNeR15nXkSAg16rXnCDXkNeR15nXkSIgPten15zXkNeV15bXoNeoIDE2LCDXqNee16og15DXkdeZ15EgINeq15wg15DXkdeZ15EgPGEvPmQCBQ9kFgICAw8WAh4LXyFJdGVtQ291bnQCEmRkM4xyJrSsUtlPrWypNTsllj7DAFLHQS2aHDHVUFgRvfE=" />

    <div class="gradientBG meetingsWrap">
        <!-- replace static texts with texts from .resx or with a protected variable/property from the page-->
        <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingsDetails.aspx?cID=20109&sem=2015%d7%92" id="mainLink" class="meetingsEventLink">
            <div class="MainRowContent">
                <div>
                    <div id="" class="MainRowTitle">
                        <div style="float: right" class="meetingText">
                            קבוצת לימוד:</div>
                    </div>
                    <div id="" class="MainRowValue">
                        <div id="GroupNumber" class="meetingText">10</div>
                    </div>
                </div>
                <div class="MainRowTitle" style="width: 100%">
                    <div id="GuidanceType" class="meetingText">הנחיה מוגברת</div>
                </div>
                <div>
                    <div id="Div3" class="MainRowTitle">
                        <div class="meetingText">
                            מפגשים</div>
                    </div>
                    <div id="Div4" class="MainRowValue">
                        <div id="HoursRange" class="meetingText"> יום ג 11:00-09:00 יום ה 11:00-09:00</div>
                    </div>
                </div>
            </div>
            <div class="MainRowArrow">
                <img alt="" class="ArrowLeftGrey" src="images/gray_arrow.png" />
            </div>
        </a>
        <div id="StudyGroupBuildingAddress" class="AddressStyle" style="width: 100%"><a href="nwtp.map://maps.google.com/maps?q=קלאוזנר 16, רמת אביב  תל אביב" >קלאוזנר 16, רמת אביב  תל אביב <a/></div>
        <div id="UpcomingMeetings">
            <div class="meetingBlueRow">
                <!--span>המפגשים הקרובים</span--></div>
            <div class="meetingGreyRow">
                <span class="firstCol">&nbsp</span> <span class="secondCol">תאריך</span> <span class="thirdCol">
                    שעה</span>
            </div>

            <ul class="meetingItem">

                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979055" style="text-decoration:none;"><li><span class="firstCol">1</span><span class="secondCol">14.07.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="1" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979056" style="text-decoration:none;"><li><span class="firstCol">2</span><span class="secondCol">16.07.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="2" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979057" style="text-decoration:none;"><li><span class="firstCol">3</span><span class="secondCol">21.07.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="3" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979058" style="text-decoration:none;"><li><span class="firstCol">4</span><span class="secondCol">23.07.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="4" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979059" style="text-decoration:none;"><li><span class="firstCol">5</span><span class="secondCol">28.07.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="5" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979060" style="text-decoration:none;"><li><span class="firstCol">6</span><span class="secondCol">30.07.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="6" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979061" style="text-decoration:none;"><li><span class="firstCol">7</span><span class="secondCol">04.08.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="7" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979062" style="text-decoration:none;"><li><span class="firstCol">8</span><span class="secondCol">06.08.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="8" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979063" style="text-decoration:none;"><li><span class="firstCol">9</span><span class="secondCol">11.08.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="9" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979064" style="text-decoration:none;"><li><span class="firstCol">10</span><span class="secondCol">13.08.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="10" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979065" style="text-decoration:none;"><li><span class="firstCol">11</span><span class="secondCol">18.08.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="11" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979066" style="text-decoration:none;"><li><span class="firstCol">12</span><span class="secondCol">20.08.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="12" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979067" style="text-decoration:none;"><li><span class="firstCol">13</span><span class="secondCol">25.08.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="13" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979068" style="text-decoration:none;"><li><span class="firstCol">14</span><span class="secondCol">27.08.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="14" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979069" style="text-decoration:none;"><li><span class="firstCol">15</span><span class="secondCol">01.09.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="15" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979070" style="text-decoration:none;"><li><span class="firstCol">16</span><span class="secondCol">03.09.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="16" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979071" style="text-decoration:none;"><li><span class="firstCol">17</span><span class="secondCol">08.09.15 (יום ג)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="17" class="ArrowLeftGrey" /></li></a>
                <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=20109&semester=2015%d7%92&mID=1979072" style="text-decoration:none;"><li><span class="firstCol">18</span><span class="secondCol">10.09.15 (יום ה)</span><span class="thirdCol">09:00 - 11:00</span><img src="images/gray_arrow.png" alt="18" class="ArrowLeftGrey" /></li></a>
            </ul>
        </div>
    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
