<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="HandheldFriendly" content="True" />
<head><title>
	Courses List
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/Courses.css" type="text/css" /></head>
<body style="float: right; direction: rtl;">
    <form name="frmMain" method="post" action="courses.aspx" id="frmMain">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTcwMzgyNjkwNw9kFgICAQ9kFgICAw8WAh4LXyFJdGVtQ291bnQCAmRkbotO9A72zCHJ1/uB6lqnP98KI22YpsRbVcO25Azd2Wc=" />

    <div class="CoursesWrap">
        <div class="Title Courses_Title">
            <h2>
                הקורסים שלי</h2>
        </div>

        <ul class="Courses_List">

                <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/coursedetails.aspx?id=20109&courseName=%d7%90%d7%9c%d7%92%d7%91%d7%a8%d7%94+%d7%9c%d7%99%d7%a0%d7%90%d7%a8%d7%99%d7%aa+1&semester=2015%d7%92"><span class="courseTitle">אלגברה לינארית 1</span><span>מס' קורס : 20109&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;סמסטר: 2015ג</span><img src="images/gray_arrow.png" alt="אלגברה לינארית 1" /></a></li>
                <li><a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/coursedetails.aspx?id=04101&courseName=%d7%90%d7%a9%d7%a0%d7%91+%d7%9c%d7%9e%d7%aa%d7%9e%d7%98%d7%99%d7%a7%d7%94&semester=2015%d7%91"><span class="courseTitle">אשנב למתמטיקה</span><span>מס' קורס : 04101&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;סמסטר: 2015ב</span><img src="images/gray_arrow.png" alt="אשנב למתמטיקה" /></a></li>
        </ul>
    </div>
    </form>
    <script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
