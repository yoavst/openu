

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1"><title>

</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/MessageDetails.css" type="text/css" /></head>
<body>
<form name="form1" method="post" action="MessageDetails.aspx?report=064&amp;order=50997&amp;subreport=01" id="form1">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE3NDYwOTk2NjgPZBYCAgEPZBYGAgEPFgIeCWlubmVyaHRtbAUx16rXmden15XXnyDXkdek16rXqNeV158g16nXnCDXldefINeT16gg157Xldeg15PXlGQCAw8WAh4FY2xhc3MFBkhpZGRlbmQCCQ8WAh4LXyFJdGVtQ291bnQCAhYEZg9kFgICAQ8WAh8ABd0BPGEgaHJlZj0iaHR0cHM6Ly9zaGVpbHRhLmFwcHMub3BlbnUuYWMuaWwvcGxzL2RteW9wdDIvZGlzcGxheV9tZXNzYWdlLmRpc3BsYXlfaHRtbD9taXNfaXNodXI9MTAwNzI3NyZTU0lEX0lOPTE2MTA4MDAyOTQ2NzU4NzU1MDg0MDYwOTI0OTY1NDE3OTEwMTgzOTE1ODI1NDc1OTk0Nzc2MzgyMDE1NTE5NTUmRF9JRF9JTj1JdElzQUZha2VJZCZJUF9JTj04LjguOC44Ij7Xp9eV15HXpTwvYT5kAgEPZBYCAgEPFgIfAAWYAjxhIGhyZWY9Imh0dHBzOi8vc2hlaWx0YS5hcHBzLm9wZW51LmFjLmlsL3Bscy9kbXlvcHQyL2Rpc3BsYXlfbWVzc2FnZS5kaXNwbGF5X2ZpbGU/aW5fZmlsZV9uYW1lPW1haWwtMTAwNzI3Ny0xNjI0MDM5LnBkZiZpbl90eXBlPTkwOTI1OTY3MjU3NTkxMjAwJlNTSURfSU49MTYxMDgwMDI5NDY3NTg3NTUwODQwNjA5MjQ5NjU0MTc5MTAxODM5MTU4MjU0NzU5OTQ3NzYzODIwMTU1MTk1NSZEX0lEX0lOPUl0SXNBRmFrZUlkJklQX0lOPTguOC44LjgiPten15XXkdelINee16bXldeo16MgMTwvYT5kZEk7afhSy119+TPMcak7NObotUY+d/CDDZU+oakVHSPC" />

    <div>
        <div id="MessageTitle" class="Title MessageTitle">תיקון בפתרון של ון דר מונדה<div class="DateStyle">תאריך:14.08.15</div></div>
        <div id="MessageSMSTitle" class="Hidden">
            <div class="SecondTitleStyle">
                <img alt="" src="images/MessageDetail/sms_icon.png" />
                <p>
                    הודעת SMS</p>
            </div>
            <div id="MessageSMSContent" class="ContentStyle">
            </div>
        </div>
        <div id="MessageEmailTitle">
            <div class="SecondTitleStyle">
                <img alt="" src="images/MessageDetail/msg_icon.png" />
                <p>
                    הודעת דוא"ל</p>
            </div>
            <div class="ContentStyle">
                <span>ראה הודעה בקבצים המצורפים</span>
            </div>
        </div>
        <br />
        <div id="MessageAttachmentsTitle" class="SecondTitleStyle">
            <img alt="" src="images/MessageDetail/files_icon.png" />
            <p>
                קבצים מצורפים</p>
        </div>

        <div class="AttachmentStyleRow">
            <span id="rpt_MessageAttachments_ctl00_AttachmentNameSpan" class="AttachemntStyleRow"><a href="https://sheilta.apps.openu.ac.il/pls/dmyopt2/display_message.display_html?mis_ishur=1007277&SSID_IN=1610800294675875508406092496541791018391582547599477638201551955&D_ID_IN=ItIsAFakeId&IP_IN=8.8.8.8">קובץ</a></span>
            <br />
        </div>

        <div class="AttachmentStyleRow">
            <span id="rpt_MessageAttachments_ctl01_AttachmentNameSpan" class="AttachemntStyleRow"><a href="https://sheilta.apps.openu.ac.il/pls/dmyopt2/display_message.display_file?in_file_name=mail-1007277-1624039.pdf&in_type=90925967257591200&SSID_IN=1610800294675875508406092496541791018391582547599477638201551955&D_ID_IN=ItIsAFakeId&IP_IN=8.8.8.8">קובץ מצורף 1</a></span>
            <br />
        </div>

    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
