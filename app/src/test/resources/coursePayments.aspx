

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
    Course payments
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/CoursePayments.css" type="text/css" /></head>
<body>
<form name="frmMain" method="post" action="coursePayments.aspx?id=04101&amp;semester=2015%u05d1" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTcwMzgyNjkwNw9kFgICAQ9kFgICAw8WAh4LXyFJdGVtQ291bnQCAWRkMaRDuaROyAPsQE82+lwd90xXQTLhGWwGcPaTfqBkOJ4=" />

    <div class="gradientBG coursesGradesWrap">
        <!-- replace static texts with texts from .resx or with a protected variable/property from the page-->
        <div class="courseGradeGreyRow">
            <span class="clm1">תאריך</span> <span class="clm2">סוג תשלום</span> <span class="clm3">
                סכום</span>
        </div>
        <ul>


            <a href="nwtp.push://sheilta.apps.openu.ac.il/opmobile/CoursePaymentDetails.aspx?courseId=04101&semester=2015%d7%91&paymentID=7425737" style="text-decoration:none;"><li><span class="clm1">&nbsp;</span><span class="clm2">שכר לימוד<span class="eventLocation">מזומן/המחאה</span></span><span class="clm3">2236</span><img src="images/gray_arrow.png" class="ArrowLeftGrey" /></li></a>
        </ul>
    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
