

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
    Event details
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/EventDetails.css" type="text/css" /></head>
<body>
<form name="frmMain" method="post" action="EventDetails.aspx?eid=505101880&amp;date=30%2f08%2f2015" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE1MTczNjk3NzUPZBYCAgEPZBYOAgEPFgQeA3NyYwUeaW1hZ2VzL2JsdWVfdGVsZXBob25lX2ljb24ucG5nHgNhbHQFCXRpbWUgaWNvbmQCAw8WAh4JaW5uZXJodG1sBSXXnteeItefINeQ15zXkteR16jXlCDXnNeZ16DXkNeo15nXqiAxZAIFD2QWAgIBDxYCHwIFCjMwLzA4LzIwMTVkAgcPFgIeB1Zpc2libGVoZAIJDxYCHwNoZAILD2QWAgIBDxYCHwIFTNen15XXqNehINeQ15zXkteR16jXlCDXnNeZ16DXkNeo15nXqiAxIDIwMTA5INeR16HXnteh15jXqCDXkjIwMTUg157XmNec15QgMTNkAg0PFgIfA2hkZHZ9t/V/dDwIF1Eg5dYh/iGZJMpoC5gCWHMvCpdhqIue" />

    <div class="gradientBG evtDetailsWrap">
        <div class="Title evtDetailsBlueRow">
            <img src="images/blue_telephone_icon.png" id="ActivityIconType" class="imgStyle" alt="time icon" />
            <span id="ActivityTitle">ממ"ן אלגברה לינארית 1</span></div>
        <!--div class="firstRow"-->
        <div class="gradientBG">
            <div id="ActivityDateDiv">
                <span class="first" >תאריך</span> <span id="ActivityDate">30/08/2015</span>
            </div>


            <div id="ActivityDetailsDiv">
                <span class="first">פרטים נוספים</span> <span id="ActivityDetails">קורס אלגברה לינארית 1 20109 בסמסטר ג2015 מטלה 13</span>
            </div>

        </div>
    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
