<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head id="Head1">
    <title></title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="HandheldFriendly" content="True" />
    <html
        xmlns="http://www.w3.org/1999/xhtml">
    <link rel="Stylesheet" href="CSS/Global.css" type="text/css" />
    <link rel="Stylesheet" href="CSS/RequestDetails.css" type="text/css" />
</head>
<body>
<form name="form1" method="post" action="RequestDetails.aspx?requestID=I003792920" id="form1">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTczMTUxOTM2D2QWAgIBD2QWEgIBD2QWBAIBDxYCHglpbm5lcmh0bWwFE9ee16HXpNeoINeU16TXoNeZ15RkAgMPFgIfAAUKSTAwMzc5MjkyMGQCAw9kFgQCAQ8WAh8ABRPXoNeV16nXkCDXlNek16DXmdeUZAIDDxYCHwAFKdeU15LXqdeqINee15jXnNeqINee15fXqdeRICjXnteeJnF1b3Q715cpZAIFD2QWBAIBDxYCHwAFCteq15DXqNeZ15pkAgMPFgIfAAUKMjMvMDgvMjAxNWQCBw9kFgQCAQ8WAh8ABQrXodee16HXmNeoZAIDDxYCHwAFBjIwMTXXkmQCCQ9kFgQCAQ8WAh8ABQjXp9eV16jXoWQCAw8WAh8ABQUyMDEwOWQCCw9kFgQCAQ8WAh8ABQ/Xntem15Eg16TXoNeZ15RkAgMPFgIfAAUd16DXp9ec15gg15DXmiDXmNeo150g16DXkdeT16dkAg0PZBYEAgEPFgIfAAUa16rXkNeo15nXmiDXntem15Eg16TXoNeZ15RkAgMPFgIfAAUKMjMvMDgvMjAxNWQCDw9kFgQCAQ8WAh8ABRnXlNee15fXnNen15Qg15TXnteY16TXnNeqZAIDDxYCHwAFKtee16jXm9eWINeU15TXmdep15LXmdedINeU15zXmdee15XXk9eZ15nXnWQCEQ8WAh4HVmlzaWJsZWhkZIuMRXKQVCpR3fblh57JMUUW7/ZRDxpHM8WTPlhS/sir" />
    <div class="applicationsList_List">
        <div id="ApplicationNumberDiv" class="FirstRow">
            <div id="ApplicationNumberTitle" class="rowTitle">מספר הפניה</div>
            <div id="ApplicationNumberValue" class="rowValue">I003792920</div>
        </div>
        <div id="ApplicationSubjectDiv" class="cell">
            <div id="ApplicationSubjectTitle" class="rowTitle">נושא הפניה</div>
            <div id="ApplicationSubjectValue" class="rowValue">הגשת מטלת מחשב (ממ&quot;ח)</div>
        </div>
        <div id="ApplicationCreationDateDiv" class="cell">
            <div id="ApplicationCreationDateTitle" class="rowTitle">תאריך</div>
            <div id="ApplicationCreationDateValue" class="rowValue">23/08/2015</div>
        </div>
        <div id="ApplicationSemesterDiv" class="cell">
            <div id="ApplicationSemesterTitle" class="rowTitle">סמסטר</div>
            <div id="ApplicationSemesterValue" class="rowValue">2015ג</div>
        </div>
        <div id="ApplicationCursDiv" class="cell">
            <div id="ApplicationCursTitle" class="rowTitle">קורס</div>
            <div id="ApplicationCursValue" class="rowValue">20109</div>
        </div>
        <div id="ApplicationStatusDiv" class="cell">
            <div id="ApplicationStatusTitle" class="rowTitle">מצב פניה</div>
            <div id="ApplicationStatusValue" class="rowValue">נקלט אך טרם נבדק</div>
        </div>
        <div id="ApplicationUpdateDateDiv" class="cell">
            <div id="ApplicationUpdateDateTitle" class="rowTitle">תאריך מצב פניה</div>
            <div id="ApplicationUpdateDateValue" class="rowValue">23/08/2015</div>
        </div>
        <div id="ApplicationDepartmentDiv" class="cell">
            <div id="ApplicationDepartmentTitle" class="rowTitle">המחלקה המטפלת</div>
            <div id="ApplicationDepartmentValue" class="rowValue">מרכז ההישגים הלימודיים</div>
        </div>
    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
    </html>
