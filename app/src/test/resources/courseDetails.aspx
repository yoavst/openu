

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><title>
    Course Details
</title><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/CourseDetails.css" type="text/css" />
    <script type="text/javascript">
        //<![CDATA[
        var oItem = new Array();
        var oTimer = new Array();
        var oLineObject = new Array();
        var oButtonObject = new Array();
        var oHArray = new Array();
        var oCurrentH = new Array();
        var iSpeed = 30;
        var index;
        function ToggleDetails(oBtnObject, iIndex) {
            //create array key
            index = iIndex;
            oBtnObject.blur();
            var sKey = "li_" + iIndex;

            oButtonObject[sKey] = oBtnObject;

            oBtnObject = oBtnObject.parentNode;
            //get the line div to hide or show
            var oParentForLine = oBtnObject.parentNode;
            if (oParentForLine != null) {
                oParentForLine = oParentForLine.getElementsByTagName("li");
                if (oParentForLine.length > 0 && iIndex < oParentForLine.length) {
                    oParentForLine = oParentForLine[iIndex];
                    if (oParentForLine != null) {
                        oParentForLine = oParentForLine.getElementsByTagName("div");
                        if (oParentForLine.length > 0) {
                            oLineObject[sKey] = oParentForLine[0];
                        }
                    }
                }
            }

            //get the content div with details to hide or show
            var oDivs = oBtnObject.getElementsByTagName("div");
            if (oDivs.length > 1) {
                oItem[sKey] = oDivs[1];
                if (oItem[sKey] != null) {
                    if (oItem[sKey].style.display == "none" || oItem[sKey].style.display == null) {
                        //display only if reduce height animation has finished
                        if (oTimer[sKey] == null) {
                            oCurrentH[sKey] = oHArray[sKey];
                            oItem[sKey].style.display = "";
                            oTimer[sKey] = setInterval(function () { IncreaseHeight(sKey); }, 100);
                        }
                    }
                    else {
                        //hide
                        if (oTimer[sKey] == null) {
                            var oH = oItem[sKey].clientHeight - 2;
                            oHArray[sKey] = oH;
                            oItem[sKey].style.height = oH + "px";
                            oTimer[sKey] = setInterval(function () { ReduceHeight(sKey); }, 100);
                        }
                    }
                }
            }
        }

        function ReduceHeight(sKey) {
            var oH = parseInt(oItem[sKey].style.height.replace("px", "")) - iSpeed;
            if (oH >= 0) {
                oItem[sKey].style.height = oH + "px";
            } else {
                oItem[sKey].style.height = "0px";
                if (oLineObject[sKey] != null) {
                    oLineObject[sKey].style.display = "none";
                }
                if (oItem[sKey] != null) {
                    oItem[sKey].style.display = "none";
                }
                if (oButtonObject[sKey] != null) {
                    var image = document.getElementById("imgDirection" + index);
                    image.setAttribute("src", "images/down_btn.png");
                    /*oButtonObject[sKey].className = "toggleBtnDown";*/
                }
                clearInterval(oTimer[sKey]);
                oTimer[sKey] = null;
                return;
            }
        }

        function IncreaseHeight(sKey) {
            var oH = parseInt(oItem[sKey].style.height.replace("px", "")) + iSpeed;
            if (oH <= oCurrentH[sKey]) {
                oItem[sKey].style.height = oH + "px";
            } else {
                oItem[sKey].style.height = oCurrentH[sKey] + "px";
                if (oLineObject[sKey] != null) {
                    oLineObject[sKey].style.display = "";
                }
                if (oButtonObject[sKey] != null) {
                    var image = document.getElementById("imgDirection" + index);
                    image.setAttribute("src", "images/up_btn.png");
                    /*oButtonObject[sKey].className = "toggleBtn";*/
                }
                clearInterval(oTimer[sKey]);
                oTimer[sKey] = null;
                return;
            }
        }
        //]]>
    </script>
</head>
<body>
<form name="frmMain" method="post" action="coursedetails.aspx?id=20109&amp;semester=2015%u05d2" id="frmMain">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMzcxNTk1NDExD2QWAgIBD2QWAgIDD2QWBAIDDxYCHgdkaXNwbGF5BQRub25lZAINDxYCHgtfIUl0ZW1Db3VudAIBZGQxP1dUzRni42LkHYckQSJjbXUI+5Xsr+0cjzNByHOmhg==" />


    <div id="maindiv" class="gradientBG courseDetaileWrap">
        <span class="title">סטטוס בקורס:</span>
        <h1>
            <div>בלימוד</div>
        </h1>
        <br />
        <ul id="ulProperties" class="toggleLines">
            <li id="PedagogicSecretaryDiv" display="none">
                <div class="linePixel">
                </div>
                <a href="javascript:void(0);" class="toggleBtn" onclick="ToggleDetails(this,1);"><span
                    class="first">מרכז הוראה</span>
                    <span>ד"ר רוסט מרים</span>
                    <img id="imgDirection1" alt="" src="images/up_btn.png" class="ImageBtnStyle" />
                </a>
                <div class="detailContent">
                    <span class="first">טלפון:</span>
                    <span class="special"><a href="nwtel://09-7781423">09-7781423</a></span>
                    <span class="first">הנחייה טלפונית:</span>
                    <span>ג   12:00-10:00</span>
                </div>
            </li>
            <li id="DetailsSecretariat">
                <div class="linePixel">
                </div>
                <a href="javascript:void(0);" class="toggleBtn" onclick="ToggleDetails(this,2);"><span
                    class="first">מזכירות</span>
                    <span>אורית זלצמן</span>
                    <img id="imgDirection2" alt="" src="images/up_btn.png" class="ImageBtnStyle" />
                </a>
                <div class="detailContent">
                    <span class="first">טלפון:</span>
                    <span class="special"><a href="nwtel://09-7781417">09-7781417</a></span>
                    <span class="first">מחלקה:</span>
                    <div class="special"><a href="http://www.openu.ac.il/Academic/CS/Math.html">מתמטיקה</a></div>
                </div>
            </li>
            <li id="DetailsLecturer">
                <div class="linePixel">
                </div>
                <a href="javascript:void(0);" class="toggleBtn" onclick="ToggleDetails(this,3);"><span
                    class="first">מנחה</span>
                    <span>ד"ר קפלן דבורה</span>
                    <img id="imgDirection3" alt="" src="images/up_btn.png" class="ImageBtnStyle" />
                </a>
                <div class="detailContent">
                    <span class="first">טלפון:</span>
                    <span class="special"><a href="nwtel://03-6774424">03-6774424</a></span>
                    <span class="first">הנחייה טלפונית</span>
                    <span>ד 21:00  -  22:00</span>
                </div>
            </li>
            <li id="DetailsAddressMaman">
                <div class="linePixel">
                </div>
                <a href="javascript:void(0);" class="toggleBtn" onclick="ToggleDetails(this,4);"><span
                    class="firstTaskEmail">כתובת למשלוח מטלות</span>
                    <img id="imgDirection4" alt="" src="images/up_btn.png" class="ImageBtnStyle" />
                </a>
                <div class="detailContent">
                    <span>הירדן 1 דירה 9 רמת גן 52281</span>
                </div>
            </li>
        </ul>
        <!--
            properties more details "blue box";
            contains the title and blue boxes with subtitle and free text
        -->
        <span id="MoreInfo" class="bottomInfoTitle">פרטים נוספים</span>

        <div class="blueBox"><div class="middle"><span class="blueBoxTitle"><BR><a href="http://opal.openu.ac.il/ouil/course.php?course=c20109&semester=2015c" >
            אתר הקורס
        </a><BR></span><span></span></div><span class="left_top"><span class="right_top"><span class="middle_top"></span></span></span></div>
    </div>
</form>
<script type="text/javascript">
    var useGA = 'true';

    if(useGA == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
