<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="HandheldFriendly" content="True" />
<head><title>
    Courses List
</title><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /><meta name="HandheldFriendly" content="True" /><link rel="Stylesheet" href="CSS/Global.css" type="text/css" /><link rel="Stylesheet" href="CSS/Courses.css" type="text/css" /></head>
<body style="float: right; direction: rtl;">
<form name="frmMain" method="post" action="courses.aspx" id="frmMain">
    <div>
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTcwMzgyNjkwNw9kFgICAQ9kFgICAQ8WAh4HVmlzaWJsZWdkZAwQZF3yjF7uIAh1z8jkV80jpdZiFmAYVdKCKqF9+Aec" />
    </div>

    <div class="CoursesWrap">
        <div class="Title Courses_Title">
            <h2>
                הקורסים שלי</h2>
        </div>
        <div id="NoCoursesDiv" class="NoCourses">
            לא נמצאו קורסים</div>
        <ul class="Courses_List">

        </ul>
    </div>
</form>
<script type="text/javascript">
    if('true' == 'true')
    {
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21931467-2']]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        }
    </script>
</body>
</html>
