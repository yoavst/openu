/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.LoginParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup


public class LoginParserTest : Spek() {
    init {
        given("a login parser") {
            val parser = LoginParser
            on("given a login document") {
                val document = Jsoup.parse("login.aspx".readFile())
                val result = parser.parse(document)
                it("should return a valid session id") {
                    shouldNotEqual(0, result.key.length())
                }
            }
            on("given a failing login document") {
                val document = Jsoup.parse("login-fail.aspx".readFile())
                val result = parser.parse(document)
                it("should return empty data") {
                   shouldEqual("", result.key)
                }
            }
        }
    }
}