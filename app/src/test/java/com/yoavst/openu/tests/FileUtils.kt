/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests
import java.io.File
import java.util.*

public fun String.getTestResource(): File = File("src/test/resources/$this")

public fun String.readFile(): String {
    val file = getTestResource()
    val fileContents = StringBuilder(file.length().toInt())
    val lineSeparator = System.getProperty("line.separator")
    Scanner(file).use { scanner ->
        while (scanner.hasNextLine()) {
            fileContents.append(scanner.nextLine()).append(lineSeparator)
        }
        return fileContents.toString()
    }
}