/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.model.RequestTitle
import com.yoavst.openu.parser.RequestsParser
import com.yoavst.openu.tests.NoExceptionThrownException
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class RequestsParserTest: Spek() {
    init {
        given("a requests parser") {
            val parser = RequestsParser

            on("trying to parse a document") {
                val document = Jsoup.parse("requests.aspx".readFile())
                val requests = parser.parse(document)
                it("should have 8 requests") {
                    shouldEqual(8, requests.size())
                }
                it("should parse the requests correctly") {
                    val randomOne = requests[3]
                    shouldEqual("הגשת מטלת מחשב...", randomOne.title)
                    shouldEqual("04101", randomOne.courseId)
                    shouldEqual("25/06/2015", randomOne.date)
                    shouldEqual("המטלה הוגשה", randomOne.status)
                    shouldEqual("I003707767", randomOne.id)
                }
            }

            on("trying to parse a document with no messages") {
                val document = Jsoup.parse("requests-empty.aspx".readFile())
                var requests: Array<RequestTitle>? = null
                it("should parse it correctly") {
                    shouldThrow(NoExceptionThrownException::class.java) {
                        requests = parser.parse(document)
                        shouldNotBeNull(requests)
                        throw NoExceptionThrownException()
                    }
                }
                it("should have no courses") {
                    shouldEqual(0, requests!!.size())
                }
            }
        }
    }
}