/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.CalendarEventParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class CalendarEventParserTest : Spek() {
    init {
        given("A calendar event parser") {
            val parser = CalendarEventParser
            on("trying to parse a document") {
                val document = Jsoup.parse("event.aspx".readFile())
                val data = parser.parse(document)
                it("should parse the data correctly") {
                    shouldNotBeNull(data)
                    shouldEqual("קורס אלגברה לינארית 1 20109 בסמסטר ג2015 מטלה 13", data.extra)
                }
            }
        }
    }
}