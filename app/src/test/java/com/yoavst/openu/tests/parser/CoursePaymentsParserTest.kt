/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.CoursePaymentsParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class CoursePaymentsParserTest : Spek() {
    init {
        given("A payment parser") {
            val parser = CoursePaymentsParser
            on("trying to parse a document") {
                val document = Jsoup.parse("coursePayments.aspx".readFile())
                val payments = parser.parse(document)
                it("should have only one payment") {
                    shouldEqual(1, payments.size())
                }
                it("should parse the payment correctly") {
                    val payment = payments.first()
                    shouldEqual("", payment.date)
                    shouldEqual("שכר לימוד מזומן/המחאה", payment.type)
                    shouldEqual("2236", payment.amount)
                    shouldEqual("https://sheilta.apps.openu.ac.il/opmobile/CoursePaymentDetails.aspx?courseId=04101&semester=2015%d7%91&paymentID=7425737", payment.link)
                }
            }
        }
    }
}