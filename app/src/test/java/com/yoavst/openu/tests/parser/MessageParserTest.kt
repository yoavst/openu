/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.MessageParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class MessageParserTest: Spek() {
    init {
        given("A message parser") {
            val parser = MessageParser
            on("trying to parse a document") {
                val document = Jsoup.parse("message.aspx".readFile())
                val message = parser.parse(document)
                it("should parse the metadata correctly") {
                    shouldEqual("14.08.15", message.date)
                    shouldEqual("תיקון בפתרון של ון דר מונדה", message.title)
                }
                it("should parse the data correctly") {
                    shouldEqual("", message.smsMessage)
                    shouldEqual("ראה הודעה בקבצים המצורפים", message.mailMessage)
                    shouldEqual(2, message.links.size())
                    shouldEqual("https://sheilta.apps.openu.ac.il/pls/dmyopt2/display_message.display_html?mis_ishur=1007277&SSID_IN=1610800294675875508406092496541791018391582547599477638201551955&D_ID_IN=ItIsAFakeId&IP_IN=8.8.8.8",
                           message.links[0].second)
                    shouldEqual("https://sheilta.apps.openu.ac.il/pls/dmyopt2/display_message.display_file?in_file_name=mail-1007277-1624039.pdf&in_type=90925967257591200&SSID_IN=1610800294675875508406092496541791018391582547599477638201551955&D_ID_IN=ItIsAFakeId&IP_IN=8.8.8.8",
                            message.links[1].second)
                }
            }
        }
    }
}