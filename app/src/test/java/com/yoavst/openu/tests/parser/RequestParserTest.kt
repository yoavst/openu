/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.RequestParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class RequestParserTest: Spek() {
    init {
        given("A request parser") {
            val parser = RequestParser
            on("trying to parse a document") {
                val document = Jsoup.parse("request.aspx".readFile())
                val request = parser.parse(document)
                it("should parse the data correctly") {
                    shouldEqual("I003792920", request.id)
                    shouldEqual("23/08/2015", request.date)
                    shouldEqual("2015ג", request.semester)
                    shouldEqual("20109", request.course)
                    shouldEqual("נקלט אך טרם נבדק", request.state)
                    shouldEqual("23/08/2015", request.stateDate)
                    shouldEqual("מרכז ההישגים הלימודיים", request.responsible)
                    shouldEqual("הגשת מטלת מחשב (ממ\"ח)",request.title)
                }
            }
        }
    }
}