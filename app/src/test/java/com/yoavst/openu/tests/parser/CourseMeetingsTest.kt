/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.CourseDetailsParser
import com.yoavst.openu.parser.CourseMeetingsParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup


public class CourseMeetingsTest : Spek() {
    init {
        given("A course details parser") {
            val parser = CourseMeetingsParser
            on("trying to parse a document") {
                val document = Jsoup.parse("courseMeetings.aspx".readFile())
                val meetings = parser.parse(document)
                it("should have the right number of meetings") {
                    shouldEqual(18, meetings.size())
                }
                it("should parse them correctly") {
                    val random = meetings[6]
                    shouldEqual("7", random.number)
                    shouldEqual("04.08.15 (יום ג)", random.date)
                    shouldEqual("09:00 - 11:00", random.hours)
                    shouldEqual("1979061", random.meetingId)
                }
            }
        }

    }
}