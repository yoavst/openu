/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.model.CalendarWeek
import com.yoavst.openu.parser.WeeklyCalendarParser
import com.yoavst.openu.tests.NoExceptionThrownException
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class WeeklyCalendarParserTest : Spek() {
    init {
        given("a weekly calendar parser") {
            val parser = WeeklyCalendarParser
            on("trying to parse a document") {
                val document = Jsoup.parse("weeklyCalendar.aspx".readFile())
                val data = parser.parse(document)
                it("should have 5 events") {
                    shouldEqual(5, data.events.size())
                }
                it("should have the right week number") {
                    shouldEqual(36, data.week)
                }
                it("should have the right dates") {
                    shouldEqual("30.08.15", data.startDate)
                    shouldEqual("05.09.15", data.endDate)
                }
                it("should parse the events correctly") {
                    val randomOne = data.events[1]
                    shouldEqual("מפגש אלגברה לינארית 1", randomOne.title)
                    shouldEqual("מרכז לימוד תל אביב - קמפוס רמת אביב 630 בקבוצה 10", randomOne.location)
                    shouldEqual("01.09.15", randomOne.date)
                    shouldEqual("09:00 - 11:00", randomOne.hours)
                    shouldEqual("-753033372", randomOne.id)
                }
            }

            on("trying to parse a document with no events") {
                val document = Jsoup.parse("weeklyCalendar-empty.aspx".readFile())
                var data: CalendarWeek?
                it("should parse it correctly") {
                    shouldThrow(NoExceptionThrownException::class.java) {
                        data = parser.parse(document)
                        shouldNotBeNull(data)
                        throw NoExceptionThrownException()
                    }
                }
                it("should have no events") {
                    shouldEqual(0, data!!.events.size())
                }
            }
        }
    }
}