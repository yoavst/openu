/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.model.CourseTitle
import com.yoavst.openu.parser.CoursesParser
import com.yoavst.openu.tests.NoExceptionThrownException
import com.yoavst.openu.tests.getTestResource
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class CoursesParserTest: Spek() {
    init {
        given("a courses parser") {
            val parser = CoursesParser
            on("trying to parse a document with two courses") {
                val document = Jsoup.parse("courses.aspx".readFile())
                val courses = parser.parse(document)
                it("should have two courses") {
                    shouldEqual(2, courses.size())
                }
                it("should parse the first course correctly") {
                    val course = courses[0]
                    shouldEqual("אלגברה לינארית 1", course.name)
                    shouldEqual("20109", course.id)
                    shouldEqual("2015ג", course.term)
                    shouldEqual("nwtp.push://sheilta.apps.openu.ac.il/opmobile/coursedetails.aspx?id=20109&courseName=%d7%90%d7%9c%d7%92%d7%91%d7%a8%d7%94+%d7%9c%d7%99%d7%a0%d7%90%d7%a8%d7%99%d7%aa+1&semester=2015%d7%92",
                            course.link)

                }
                it("should parse the second course correctly") {
                    val course = courses[1]
                    shouldEqual("אשנב למתמטיקה", course.name)
                    shouldEqual("04101", course.id)
                    shouldEqual("2015ב", course.term)
                    shouldEqual("nwtp.push://sheilta.apps.openu.ac.il/opmobile/coursedetails.aspx?id=04101&courseName=%d7%90%d7%a9%d7%a0%d7%91+%d7%9c%d7%9e%d7%aa%d7%9e%d7%98%d7%99%d7%a7%d7%94&semester=2015%d7%91",
                            course.link)

                }
            }
            on("trying to parse a document with no courses") {
                val document = Jsoup.parse("courses-empty.aspx".readFile())
                var courses: Array<CourseTitle>? = null
                it("should parse it correctly") {
                    shouldThrow(NoExceptionThrownException::class.java) {
                        courses = parser.parse(document)
                        shouldNotBeNull(courses)
                        throw NoExceptionThrownException()
                    }
                }
                it("should have no courses") {
                    shouldEqual(0, courses!!.size())
                }
            }
        }
    }
}