/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.CourseGradesParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class CourseGradesTest: Spek() {
    init {
        given("A grades parser") {
            val parser = CourseGradesParser
            on("trying to parse a document") {
                val document = Jsoup.parse("courseGrades.aspx".readFile())
                val grades = parser.parse(document)
                it("should parse the data correctly") {
                    shouldNotBeNull(grades)
                    shouldEqual(10, grades.size())
                }
                it("should parse a random data correctly") {
                    val random = grades[6]
                    shouldEqual("12", random.id)
                    shouldEqual("ממ\"ן", random.type)
                    shouldEqual("81", random.grade)
                    shouldEqual("29.04.15", random.date)
                }
            }
        }
    }
}