/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.model.MessageTitle
import com.yoavst.openu.parser.MessagesParser
import com.yoavst.openu.tests.NoExceptionThrownException
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class MessagesParserTest : Spek() {
    init {
        given("a message parser") {
            val parser = MessagesParser
            on("trying to parse a document") {
                val document = Jsoup.parse("messages.aspx".readFile())
                val messages = parser.parse(document)
                it("should have 54 messages") {
                    shouldEqual(54, messages.size())
                }
                it("should parse them correctly") {
                    val randomOne =  messages[29]
                    shouldEqual("קורס : אלגברה לינארי...", randomOne.title)
                    shouldEqual("05/07/2015", randomOne.date)
                    shouldEqual("https://sheilta.apps.openu.ac.il/opmobile/MessageDetails.aspx?report=111&order=B1371&subreport=", randomOne.link)
                }
            }
            on("trying to parse a document with no messages") {
                val document = Jsoup.parse("messages-empty.aspx".readFile())
                var messages: Array<MessageTitle>? = null
                it("should parse it correctly") {
                    shouldThrow(NoExceptionThrownException::class.java) {
                        messages = parser.parse(document)
                        shouldNotBeNull(messages)
                        throw NoExceptionThrownException()
                    }
                }
                it("should have no courses") {
                    shouldEqual(0, messages!!.size())
                }
            }
        }
    }
}