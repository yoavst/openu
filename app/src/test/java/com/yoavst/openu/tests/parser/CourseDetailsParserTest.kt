/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.tests.parser

import com.yoavst.openu.parser.CourseDetailsParser
import com.yoavst.openu.tests.readFile
import org.jsoup.Jsoup

public class CourseDetailsParserTest: Spek() {
    init {
        given("A course details parser") {
            val parser = CourseDetailsParser
            on("trying to parse a document") {
                val document = Jsoup.parse("courseDetails.aspx".readFile())
                val details = parser.parse(document)
                it("should parse all the data correctly") {
                    shouldEqual("בלימוד", details.status)
                    shouldEqual("ד\"ר רוסט מרים", details.teachingCenter)
                    shouldEqual("09-7781423",details.teachingCenterPhone)
                    shouldEqual("ג 12:00-10:00",details.phoneTeaching)
                    shouldEqual("אורית זלצמן",details.secretariat)
                    shouldEqual("09-7781417",details.secretariatPhone)
                    shouldEqual("מתמטיקה",details.secretariatClass)
                    shouldEqual("ד\"ר קפלן דבורה",details.teacher)
                    shouldEqual("03-6774424",details.teacherPhone)
                    shouldEqual("ד 21:00 - 22:00",details.teacherPhoneTeaching)
                    shouldEqual("הירדן 1 דירה 9 רמת גן 52281",details.addressForSending)
                    shouldEqual("http://opal.openu.ac.il/ouil/course.php?course=c20109&semester=2015c",details.courseSite)
                }
            }
        }

    }
}