/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.controller

import com.chibatching.kotpref.Kotpref
import com.chibatching.kotpref.KotprefModel
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.yoavst.openu.android.toCalendar
import com.yoavst.openu.model.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


public object DatabaseControllerImpl : KotprefModel(), DatabaseController {
    override var username: String  by stringPrefVar()
    override var password: String  by stringPrefVar()
    override var id: String by stringPrefVar()
    override var session: String by stringPrefVar()
    override var deviceId: String by stringPrefVar()

    private var ICourses: String by stringPrefVar("")
    private var IMessages: String by stringPrefVar("")
    private var IRequests: String by stringPrefVar("")

    override var courses: Array<CourseTitle>?
        get() {
            val data = ICourses
            if (data.isEmpty()) return null
            else
                return gson.fromJson(data)
        }
        set(value) {
            if (value == null) ICourses = ""
            else ICourses = gson.toJson(value)
        }
    override var messages: Array<MessageTitle>?
        get() {
            val data = IMessages
            if (data.isEmpty()) return null
            else
                return gson.fromJson(data)
        }
        set(value) {
            if (value == null) IMessages = ""
            else IMessages = gson.toJson(value)
        }
    override var requests: Array<RequestTitle>?
        get() {
            val data = IRequests
            if (data.isEmpty()) return null
            else
                return gson.fromJson(data)
        }
        set(value) {
            if (value == null) IRequests = ""
            else IRequests = gson.toJson(value)
        }


    override fun hasEnoughCache(): Boolean {
        return ICourses.isNotEmpty() && IMessages.isNotEmpty() && IRequests.isNotEmpty() && getValidWeekFile(CalendarFileFormat.format(Date()))?.length() ?: 0 != 0L
    }

    override fun getWeek(day: Int, month: Int, year: Int): CalendarWeek? {
        val cal = Calendar.getInstance() apply { set(year, month - 1, day) }
        val file = getValidWeekFile(CalendarFileFormat.format(cal.time)) ?: return null
        return readWeekFile(file)
    }

    override fun setWeek(data: CalendarWeek?) {
        if (data != null) {
            (getWeekFile(data.startDate) apply { createNewFile() }).writeText(gson.toJson(data))
        }
    }

    override fun bulk(changing: DatabaseController.() -> Unit) {
        Kotpref.bulk(DatabaseControllerImpl) {
            changing()
        }
    }

    public override fun clearData() {
        clear()
        CalendarDirectory.deleteRecursively()
        RequestsDirectory.deleteRecursively()
        init()
    }

    override fun getRequest(title: RequestTitle): RequestData? {
        return readRequestFile(getRequestFileName(title.id))
    }

    override fun setRequest(data: RequestData) {
        getRequestFileName(data.id).writeText(gson.toJson(data))
    }

    override fun getMessageData(title: MessageTitle): MessageData? {
        return readMessageFile(getMessageFileName(title.id))
    }

    override fun setMessageDate(title: MessageTitle, data: MessageData) {
        getMessageFileName(title.id).writeText(gson.toJson(data))
    }

    override fun getCalendarExtra(title: CalendarTitle): String? {
        val id = title.id
        var index: Int
        if (CalendarExtraFile.length() != 0L)
            CalendarExtraFile.readText().split('\n') forEach {
                index = it.indexOf('|')
                if (index != -1)
                    if (it.substring(0, index) == id)
                        return it.substring(index + 1)
            }
        return null
    }

    override fun SetCalendarExtra(title: CalendarTitle, value: String) {
        // setting is one time only (for performance), therefore setting the title more then once will not work and may cause error.
        CalendarExtraFile.appendText(title.id + "|" + value + '\n')
    }

    override fun setCourseData(courseData: CourseData) {
        getCourseDataFilename(courseData.title.id).writeText(gson.toJson(courseData))
    }

    override fun getCourseData(id: String): CourseData? {
        return readCourseDataFile(getCourseDataFilename(id))
    }

    override fun hasCourseData(courseTitle: CourseTitle): Boolean {
        return getCourseDataFilename(courseTitle.id).exists()
    }

    //region Course data logic
    private fun getCourseDataFilename(id: String): File {
        return File(CoursesDirectory, id + ".json")
    }

    private fun readCourseDataFile(file: File): CourseData? {
        if (file.length() == 0L) return null
        else return gson.fromJson<CourseData>(file.readText())
    }
    //endregion

    //region Message logic
    private fun getMessageFileName(id: Int): File {
        return File(MessagesDirectory, id.toString() + ".json")
    }

    private fun readMessageFile(file: File): MessageData? {
        if (file.length() == 0L) return null
        else return gson.fromJson<MessageData>(file.readText())
    }
    //endregion

    //region Request logic
    private fun getRequestFileName(id: String): File {
        return File(RequestsDirectory, id + ".json")
    }

    private fun readRequestFile(file: File): RequestData? {
        if (file.length() == 0L) return null
        else return gson.fromJson<RequestData>(file.readText())
    }
    //endregion

    //region Week logic
    private fun getValidWeekFile(name: String): File? {
        val date = CalendarFileFormat.parse(name)
        val cal = date.toCalendar() apply { set(Calendar.DAY_OF_WEEK, firstDayOfWeek) }
        val file = File(CalendarDirectory, CalendarFileFormat.format(cal.time) + ".json")
        if (file.exists())
            return file
        else return null
    }

    private fun getWeekFile(name: String): File {
        return File(CalendarDirectory, name + ".json")
    }

    private fun readWeekFile(file: File): CalendarWeek? {
        if (file.length() == 0L) return null
        else return gson.fromJson<CalendarWeek>(file.readText())
    }
    //endregion

    private val CalendarDirectory = File("/data/data/com.yoavst.openu/files/calendar/")
    private val RequestsDirectory = File("/data/data/com.yoavst.openu/files/request/")
    private val MessagesDirectory = File("/data/data/com.yoavst.openu/files/messages/")
    private val CoursesDirectory = File("/data/data/com.yoavst.openu/files/courses/")

    private val CalendarExtraFile = File(CalendarDirectory, "extra.txt")
    private val CalendarFileFormat = SimpleDateFormat("dd.MM.yy")

    private val gson = Gson()

    init {
        init()
    }

    fun init() {
        CalendarDirectory.mkdirs()
        RequestsDirectory.mkdirs()
        MessagesDirectory.mkdirs()
        CoursesDirectory.mkdirs()
        CalendarExtraFile.createNewFile()
    }

}
