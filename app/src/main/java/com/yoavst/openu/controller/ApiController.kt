/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.controller

import com.yoavst.openu.model.*
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.combine.combine
import nl.komponents.kovenant.then
import java.text.SimpleDateFormat

public interface ApiController {
    fun getCourses(userId: String, sessionId: String, deviceId: String, ip: String): Promise<Array<CourseTitle>, Exception>
    fun getMessages(userId: String, sessionId: String, deviceId: String, ip: String): Promise<Array<MessageTitle>, Exception>
    fun getRequests(userId: String, sessionId: String, deviceId: String, ip: String): Promise<Array<RequestTitle>, Exception>
    fun getWeeklyCalendar(userId: String, sessionId: String, deviceId: String, ip: String, startDate: String? = null): Promise<CalendarWeek, Exception>
    fun login(userId: String, username: String, password: String, deviceId: String, deviceIp: String): Promise<String, Exception>
    fun getRequest(userId: String, sessionId: String, deviceId: String, ip: String, title: RequestTitle): Promise<RequestData, Exception>
    fun getCalendarExtra(userId: String, sessionId: String, deviceId: String, ip: String, title: CalendarTitle): Promise<String, Exception>
    fun getMessage(userId: String, sessionId: String, deviceId: String, ip: String, title: MessageTitle): Promise<MessageData, Exception>
    fun getCourseDetails(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<CourseDetails, Exception>
    fun getCoursePayments(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<Array<CoursePayment>, Exception>
    fun getCourseGrades(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<Array<CourseGrade>, Exception>
    fun getCourseMeetings(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<Array<CourseMeeting>, Exception>

    fun getCourseData(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<CourseData, Exception> {
        return combine(getCourseDetails(userId, sessionId, deviceId, ip, title),
                getCoursePayments(userId, sessionId, deviceId, ip, title),
                getCourseGrades(userId, sessionId, deviceId, ip, title),
                getCourseMeetings(userId, sessionId, deviceId, ip, title)) then { CourseData(title, it.first, it.fourth, it.third, it.second) }
    }

    companion object {
        public val FakeIp: String = "8.8.8.8" // "Moshe, google seem to be interested about us"
        public val CalendarDateFormat: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
    }
}