/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.controller

import com.yoavst.openu.model.*
import com.yoavst.openu.parser.Parser
import com.yoavst.openu.soap.Soap
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.then
import org.jsoup.Jsoup
import uy.kohesive.injekt.injectLazy
import org.jsoup.parser.Parser as JsoupParser


public object ApiControllerImpl : ApiController {
    private val requestController: RequestsController by injectLazy()

    private val Version = "1.0"
    private val UserAgent = "Doctor Who"

    //region Urls
    private val MessagesUrl = "https://sheilta.apps.openu.ac.il/opmobile/Messages.aspx"
    private val RequestsUrl = "https://sheilta.apps.openu.ac.il/opmobile/Requests.aspx"
    private val WeeklyCalendarUrl = "https://sheilta.apps.openu.ac.il/opmobile/WeeklyCalendar.aspx"
    private val CoursesUrl = "https://sheilta.apps.openu.ac.il/opmobile/courses.aspx"
    private val LoginUrl = "https://sheilta.apps.openu.ac.il/opmobilews/login.asmx"
    private fun weeklyCalendarUrl(date: String) = "https://sheilta.apps.openu.ac.il/opmobile/WeeklyCalendar.aspx?date=$date"
    private fun requestUrl(id: String) = "https://sheilta.apps.openu.ac.il/opmobile/RequestDetails.aspx?requestID=$id"
    private fun calendarEventUrl(id: String, date: String) = "https://sheilta.apps.openu.ac.il/opmobile/EventDetails.aspx?eid=$id&date=$date"
    private fun courseDetailsUrl(id: String, semester: String) = "https://sheilta.apps.openu.ac.il/opmobile/courseDetails.aspx?id=$id&semester=$semester"
    private fun coursePaymentsUrl(id: String, semester: String) = "https://sheilta.apps.openu.ac.il/opmobile/coursePayments.aspx?id=$id&semester=$semester"
    private fun courseGradesUrl(id: String, semester: String) = "https://sheilta.apps.openu.ac.il/opmobile/courseGrades.aspx?id=$id&semester=$semester"
    private fun courseMeetingsUrl(id: String, semester: String) = "https://sheilta.apps.openu.ac.il/opmobile/courseMeetings.aspx?id=$id&semester=$semester"
    private fun courseGradeUrl(courseId: String, semester: String, id: String) = "https://sheilta.apps.openu.ac.il/opmobile/CourseGradeDetails.aspx?cId=$courseId&semester=$semester&gid=$id"
    private fun courseMeetingUrl(courseId: String, semester: String, id: String) = "https://sheilta.apps.openu.ac.il/opmobile/CourseMeetingDetails.aspx?cID=$courseId&semester=$semester&mID=$id"
    //endregion

    //region Parsers
    private val CoursesParser: Parser<Array<CourseTitle>> by injectLazy()
    private val MessagesParser: Parser<Array<MessageTitle>> by injectLazy()
    private val RequestsParser: Parser<Array<RequestTitle>> by injectLazy()
    private val WeeklyCalendarParser: Parser<CalendarWeek> by injectLazy()
    private val CalendarExtraParser: Parser<CalendarEventExtra> by injectLazy()
    private val CourseDetailsParser: Parser<CourseDetails> by injectLazy()
    private val CoursePaymentsParser: Parser<Array<CoursePayment>> by injectLazy()
    private val CourseGradesParser: Parser<Array<CourseGrade>> by injectLazy()
    private val CourseMeetingParser: Parser<Array<CourseMeeting>> by injectLazy()
    private val LoginParser: Parser<Session> by injectLazy()
    private val RequestParser: Parser<RequestData> by injectLazy()
    private val MessageParser: Parser<MessageData> by injectLazy()
    //endregion


    public override fun getCourses(userId: String, sessionId: String, deviceId: String, ip: String): Promise<Array<CourseTitle>, Exception> {
        return get(CoursesUrl, userId, sessionId, deviceId, ip) then {
            CoursesParser.parse(Jsoup.parse(it.second))
        }
    }

    public override fun getMessages(userId: String, sessionId: String, deviceId: String, ip: String): Promise<Array<MessageTitle>, Exception> {
        return get(MessagesUrl, userId, sessionId, deviceId, ip) then {
            MessagesParser.parse(Jsoup.parse(it.second))
        }
    }

    public override fun getRequests(userId: String, sessionId: String, deviceId: String, ip: String): Promise<Array<RequestTitle>, Exception> {
        return get(RequestsUrl, userId, sessionId, deviceId, ip) then {
            RequestsParser.parse(Jsoup.parse(it.second))
        }
    }

    public override fun getWeeklyCalendar(userId: String, sessionId: String, deviceId: String, ip: String, startDate: String?): Promise<CalendarWeek, Exception> {
        return get(if (startDate != null) weeklyCalendarUrl(startDate) else WeeklyCalendarUrl, userId, sessionId, deviceId, ip) then {
            WeeklyCalendarParser.parse(Jsoup.parse(it.second))
        }
    }

    override fun getCalendarExtra(userId: String, sessionId: String, deviceId: String, ip: String, title: CalendarTitle): Promise<String, Exception> {
        return get(calendarEventUrl(title.id, CalendarTitle.ExtraData_DateFormat.format(CalendarTitle.DateFormat.parse(title.date))), userId, sessionId, deviceId, ip) then {
            CalendarExtraParser.parse(Jsoup.parse(it.second)).extra
        }
    }

    override fun getCourseDetails(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<CourseDetails, Exception> {
        return get(courseDetailsUrl(title.id, title.term), userId, sessionId, deviceId, ip) then {
            CourseDetailsParser.parse(Jsoup.parse(it.second))
        }
    }

    override fun getCoursePayments(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<Array<CoursePayment>, Exception> {
        return get(coursePaymentsUrl(title.id, title.term), userId, sessionId, deviceId, ip) then {
            CoursePaymentsParser.parse(Jsoup.parse(it.second))
        }
    }

    override fun getCourseGrades(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<Array<CourseGrade>, Exception> {
        return get(courseGradesUrl(title.id, title.term), userId, sessionId, deviceId, ip) then {
            CourseGradesParser.parse(Jsoup.parse(it.second))
        }
    }

    override fun getCourseMeetings(userId: String, sessionId: String, deviceId: String, ip: String, title: CourseTitle): Promise<Array<CourseMeeting>, Exception> {
        return get(courseMeetingsUrl(title.id, title.term), userId, sessionId, deviceId, ip) then {
            CourseMeetingParser.parse(Jsoup.parse(it.second))
        }
    }

    public override fun login(userId: String, username: String, password: String, deviceId: String, deviceIp: String): Promise<String, Exception> {
        return requestController.post(LoginUrl, mapOf(
                "Content-Type" to "text/xml",
                "User-Agent" to UserAgent,
                "SOAPAction" to "http://OU.org/Login"),
                body = soap {
                    tag("Login") {
                        attr("xmlns", "http://OU.org/")
                        tag("username", username)
                        tag("password", password)
                        tag("userID", userId)
                        tag("deviceID", deviceId)
                        tag("deviceIP", deviceIp) // I know asp.net sucks, but really, no "get ip" method?
                    }
                }.toString()) then {
            val result = LoginParser.parse(Jsoup.parse(it.second, "", JsoupParser.xmlParser())).key
            if (result.isEmpty()) throw ConnectionException("code: ${it.first} :: body: ${it.second}")
            else result
        }
    }

    override fun getRequest(userId: String, sessionId: String, deviceId: String, ip: String, title: RequestTitle): Promise<RequestData, Exception> {
        return get(requestUrl(title.id), userId, sessionId, deviceId, ip) then {
            RequestParser.parse(Jsoup.parse(it.second))
        }
    }

    override fun getMessage(userId: String, sessionId: String, deviceId: String, ip: String, title: MessageTitle): Promise<MessageData, Exception> {
        return get(title.link, userId, sessionId, deviceId, ip) then {
            MessageParser.parse(Jsoup.parse(it.second))
        }
    }

    //region Helper methods
    private fun get(url: String, userId: String, sessionId: String, deviceId: String, ip: String): Promise<Pair<Int, String>, Exception> {
        return requestController.post(url, getHeaders(userId, sessionId, deviceId, ip))
    }

    private fun getHeaders(userId: String, sessionId: String, deviceId: String, ip: String): Map<String, String> {
        return mapOf(
                "X-UID" to userId,
                "X-IP" to ip,
                "X-SID" to sessionId,
                "X-DEVICEID" to deviceId,
                "X-VERSION" to Version,
                "User-Agent" to UserAgent)
    }

    private fun soap(init: Soap.() -> Unit): Soap {
        val soap = Soap()
        soap.init()
        return soap
    }
    //endregion
}