/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.controller

import com.yoavst.openu.model.*
import java.util.Calendar


public interface DatabaseController {
    public var username: String
    public var password: String
    public var id: String
    public var session: String
    public var deviceId: String

    public var courses: Array<CourseTitle>?
    public var messages: Array<MessageTitle>?
    public var requests: Array<RequestTitle>?
    public val currentWeek: CalendarWeek?
        get() {
            val calendar = Calendar.getInstance()
            return getWeek(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR))
        }

    public fun getWeek(day: Int, month: Int, year: Int): CalendarWeek?
    public fun setWeek(data: CalendarWeek?)

    public fun getRequest(title: RequestTitle): RequestData?
    public fun setRequest(data: RequestData)

    public fun getMessageData(title: MessageTitle): MessageData?
    public fun setMessageDate(title: MessageTitle, data: MessageData)

    public fun getCalendarExtra(title: CalendarTitle): String?
    public fun SetCalendarExtra(title: CalendarTitle, value: String)

    public fun setCourseData(courseData: CourseData)
    public fun getCourseData(courseTitle: CourseTitle): CourseData? = getCourseData(courseTitle.id)
    public fun getCourseData(id: String): CourseData?
    public fun hasCourseData(courseTitle: CourseTitle): Boolean

    public fun hasEnoughCache(): Boolean

    public fun bulk(changing: DatabaseController.() -> Unit)

    public fun clearData()
}