/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.controller

import nl.komponents.kovenant.combine.combine
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import uy.kohesive.injekt.injectLazy

public object RefreshControllerImpl : RefreshController {
    val callbacks: MutableList<RefreshController.Refresher> = arrayListOf()
    val isRefreshing: Boolean = false
    val databaseController: DatabaseController by injectLazy()
    val requestsController: ApiController by injectLazy()

    override fun refresh(id: String, session: String) {
        if (!isRefreshing) {
            val deviceId = databaseController.deviceId
            combine(requestsController.getCourses(id, session, deviceId, ApiController.FakeIp),
                    requestsController.getMessages(id, session, deviceId, ApiController.FakeIp),
                    requestsController.getRequests(id, session, deviceId, ApiController.FakeIp),
                    requestsController.getWeeklyCalendar(id, session, deviceId, ApiController.FakeIp)) successUi {
                databaseController.bulk {
                    courses = it.first
                    messages = it.second
                    requests = it.third
                    setWeek(it.fourth)
                }
            } successUi {
                callbacks.forEach { it.refreshed() }
            } failUi {
                it.printStackTrace()
                callbacks.forEach { callback -> callback.failedToRefresh() }
            }
        }

    }

    override fun addCallback(refresher: RefreshController.Refresher) {
        callbacks.add(refresher)
    }

    override fun removeCallback(refresher: RefreshController.Refresher) {
        callbacks.remove(refresher)
    }
}