/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.controller

import com.yoavst.openu.model.ConnectionException
import fuel.core.Either
import fuel.core.FuelError
import fuel.core.Request
import fuel.httpPost
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.deferred

public object RequestsControllerImpl : RequestsController {
    public override fun post(url: String, headers: Map<String, String>, body: String?): Promise<Pair<Int, String>, Exception> {
        val request = url.httpPost().header(headers)
        if (body != null) request.body(body)
        return request.promise()
    }

    private fun Request.promise(): Promise<Pair<Int, String>, Exception> {
        val deferred = deferred<Pair<Int, String>, Exception>()
        this.responseString { request, response, either ->
            when (either) {
                is Either.Left<FuelError, String> -> deferred.reject(either.get())
                is Either.Right<FuelError, String> -> {
                    if (response.httpStatusCode == 200)
                        deferred.resolve(response.httpStatusCode to either.get())
                    else
                        deferred.reject(ConnectionException("code: ${response.httpStatusCode} :: body: ${either.get<String>()}"))
                }
            }
        }
        return deferred.promise
    }
}