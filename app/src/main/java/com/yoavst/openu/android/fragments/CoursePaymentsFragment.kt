/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.CoursePaymentsAdapter
import com.yoavst.openu.model.CoursePayment
import com.yoavst.openu.model.VisibleItem

public class CoursePaymentsFragment : BaseListFragment(), CourseFragment {
    override fun init() {
        super.init()
        showContent()
        val payments = data.payments
        if (payments.isEmpty())
            showEmpty(R.drawable.ic_payment, getString(R.string.no_payments), "")
        else {
            val callback = { grade: CoursePayment ->

            }
            setAdapter(CoursePaymentsAdapter(activity, (payments map { VisibleItem(it) }).toTypedArray(), callback))
        }
    }
}