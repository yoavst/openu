/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.adapters

import android.app.Fragment
import android.app.FragmentManager
import android.support.v13.app.FragmentPagerAdapter
import com.yoavst.openu.R
import com.yoavst.openu.android.fragments.CalendarFragment
import com.yoavst.openu.android.fragments.CoursesFragment
import com.yoavst.openu.android.fragments.MessagesFragment
import com.yoavst.openu.android.fragments.RequestsFragment


public class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> RequestsFragment()
            1 -> MessagesFragment()
            2 -> CoursesFragment()
            3 -> CalendarFragment()
            else -> throw IllegalArgumentException()
        }
    }

    override fun getCount(): Int = 4

    companion object {
        public val TabsIcons: IntArray = intArrayOf(R.drawable.ic_assignment, R.drawable.ic_message, R.drawable.ic_class, R.drawable.ic_event)
    }
}