package com.yoavst.openu.android.fragments

import android.app.Activity
import android.app.Fragment
import com.yoavst.openu.model.CourseData
import com.yoavst.openu.model.Provider

public interface CourseFragment {
    fun getActivity(): Activity

    protected val data: CourseData
        @Suppress("UNCHECKED_CAST")
        get() = (getActivity() as Provider<CourseData>).get()
}