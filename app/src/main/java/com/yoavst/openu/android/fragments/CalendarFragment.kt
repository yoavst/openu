/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.app.ProgressDialog
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.openu.R
import com.yoavst.openu.android.*
import com.yoavst.openu.android.adapters.CalendarAdapter
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.model.CalendarTitle
import com.yoavst.openu.model.CalendarWeek
import com.yoavst.openu.model.VisibleItem
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.toast
import java.util.*

public class CalendarFragment : BaseListFragment() {
    override val layoutId: Int = R.layout.fragment_calendar
    private val date by stringResource(R.string.date)
    private val location by stringResource(R.string.learning_place)
    private val hours by stringResource(R.string.hours)
    private val extraDetails by stringResource(R.string.extra_details)

    private val weekText by lazy { bottomBar.findViewById(R.id.weekText) as TextView }
    private val bottomBar by lazy { activity.findViewById(R.id.bottomBar) as ViewGroup }
    private val next by lazy { bottomBar.findViewById(R.id.next) }
    private val last by lazy { bottomBar.findViewById(R.id.last) }
    private val weekDays by stringArrayResource(R.array.week_days)

    private val delimiter: String = ": "

    private var currentDialog: ProgressDialog? = null
    private var currentRequestDialog: AlertDialog? = null
    private var currentSnackbar: Snackbar? = null
    private var currentShownWeek: CalendarWeek? = null
    private var shouldShowBottomBar = false


    override fun init() {
        super.init()
        setWeekData(databaseController.currentWeek)

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (activity != null) {
            if (!shouldShowBottomBar || !isVisibleToUser)
                bottomBar.hide()
            else bottomBar.show()
        }
    }

    private fun setWeekData(week: CalendarWeek?) {
        currentShownWeek = week
        showContent()
        if (week == null) {
            showEmpty(R.drawable.ic_error, getString(R.string.error), getString(R.string.please_refresh))
            bottomBar.hide()
            shouldShowBottomBar = false
        } else {
            shouldShowBottomBar = true
            weekText.text = week.startDate + "-" + week.endDate
            if (week.events.isEmpty()) {
                showEmpty(R.drawable.ic_event, getString(R.string.no_events), "")
            } else {
                val callback = { event: CalendarTitle ->
                    val extra = databaseController.getCalendarExtra(event)
                    if (extra != null) {
                        showDialog(event, extra)
                    } else loadData(event)
                }
                val events: MutableList<VisibleItem<CalendarTitle>> = ArrayList((week.events.size() * 1.5).toInt())
                var lastDate: String? = null
                for (event in week.events) {
                    if (event.date != lastDate) {
                        val dayName = weekDays[event.date.parse().get(Calendar.DAY_OF_WEEK) - 1]
                        events.add(VisibleItem(title = dayName + "|" + event.date))
                        lastDate = event.date
                    }
                    events.add(VisibleItem(event))
                }
                setAdapter(CalendarAdapter(activity, events.toTypedArray(), callback))
            }
        }
    }

    private fun showDialog(title: CalendarTitle, extra: String) {
        dismissDialogs()
        currentRequestDialog = AlertDialog.Builder(activity)
                .setTitle(title.title)
                .setMessage(getDialogText(title, extra))
                .setCancelable(true)
                .setPositiveButton(R.string.confirmation) { a, b -> }
                .create() apply { show() }
    }

    private fun getDialogText(data: CalendarTitle, extra: String): CharSequence {
        return StringBuilder {
            if (data.date.isNotEmpty())
                append(date).append(delimiter).append(data.date).append('\n')
            if (data.hours.trim().isNotEmpty())
                append(hours).append(delimiter).append(data.hours).append('\n')
            if (data.location.isNotEmpty())
                append(location).append(delimiter).append(data.location).append('\n')
            if (extra.isNotEmpty())
                append(extraDetails).append(delimiter).append(extra).append('\n')
        }
    }

    private fun loadData(request: CalendarTitle) {
        dismissDialogs()
        currentDialog = ProgressDialog.show(activity, getString(R.string.refreshing), "")
        apiController.getCalendarExtra(databaseController.id, databaseController.session, databaseController.deviceId, ApiController.FakeIp, request) success {
            databaseController.SetCalendarExtra(request, it)
        } successUi {
            dismissDialogs()
            showDialog(request, it)
        } failUi {
            dismissDialogs()
            toast(R.string.refresh_error)
        }
    }

    override fun initOnce() {
        next.setOnClickListener {
            val cal = currentShownWeek?.endDate?.parse()?.apply({ add(Calendar.DAY_OF_YEAR, 1) }) ?: return@setOnClickListener
            buttonCallback(cal)
        }
        last.setOnClickListener {
            val cal = currentShownWeek?.startDate?.parse()?.apply({ add(Calendar.DAY_OF_YEAR, -7) }) ?: return@setOnClickListener
            buttonCallback(cal)
        }
        weekText.setOnClickListener {
            val cal = databaseController.currentWeek?.endDate?.parse()?.apply({ add(Calendar.DAY_OF_YEAR, 1) }) ?: return@setOnClickListener
            buttonCallback(cal, shouldShowSnackbar = false)
            toast(R.string.this_week)
        }
    }

    private fun buttonCallback(cal: Calendar, shouldShowSnackbar: Boolean = true) {
        val week = databaseController.getWeek(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR))
        if (week != null) {
            setWeekData(week)
            dismissSnackbar()
            if (shouldShowSnackbar)
                currentSnackbar = Snackbar.make(activity.findViewById(R.id.coordinatorLayout), R.string.should_update_this, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.refresh) {
                            updateWeek(cal.format())
                        } apply { show() }
        } else updateWeek(cal.format())
    }

    private fun updateWeek(startDate: String) {
        dismissDialogs()
        dismissSnackbar()
        currentDialog = ProgressDialog.show(activity, getString(R.string.refreshing), "")
        apiController.getWeeklyCalendar(databaseController.id, databaseController.session, databaseController.deviceId, ApiController.FakeIp, startDate) success {
            databaseController.setWeek(it)
        } successUi {
            dismissDialogs()
            dismissSnackbar()
            setWeekData(it)
        } failUi {
            dismissDialogs()
            toast(R.string.refresh_error)
        }
    }

    override fun onPause() {
        super.onPause()
        dismissDialogs()
    }

    private fun dismissDialogs() {
        currentDialog?.dismiss()
        currentDialog = null
        currentRequestDialog?.dismiss()
        currentRequestDialog = null
    }

    private fun dismissSnackbar() {
        currentSnackbar?.dismiss()
        currentSnackbar = null
    }

    private fun String.parse(): Calendar = CalendarTitle.DateFormat.parse(this).toCalendar()

    private fun Calendar.format(): String = ApiController.CalendarDateFormat.format(time)
}