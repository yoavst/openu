/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yoavst.openu.controller.RefreshController
import uy.kohesive.injekt.injectLazy
import kotlin.properties.Delegates

public abstract class BaseFragment : Fragment(), RefreshController.Refresher {
    private val refreshController: RefreshController by injectLazy()
    protected abstract val layoutId: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initOnce()
        init()
    }


    override fun onStart() {
        super.onStart()
        refreshController.addCallback(this)
    }

    override fun onStop() {
        super.onStop()
        refreshController.removeCallback(this)
    }

    protected abstract fun init()
    protected open fun initOnce() {}

    override fun refreshed() {
        init()
    }

    override fun failedToRefresh() {
        // the activity should manage the failing.
    }
}