/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.app.ProgressDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.support.v7.app.AlertDialog
import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.HeaderAdapter
import com.yoavst.openu.android.adapters.RequestsAdapter
import com.yoavst.openu.android.stringResource
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.model.RequestData
import com.yoavst.openu.model.RequestTitle
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*


public class RequestsFragment : BaseListFragment() {
    private val thisMonth by stringResource(R.string.this_month)
    private val older by stringResource(R.string.older)

    private val requestNumber by stringResource(R.string.request_number)
    private val date by stringResource(R.string.date)
    private val semester by stringResource(R.string.semester)
    private val course by stringResource(R.string.course)
    private val requestState by stringResource(R.string.request_state)
    private val stateDate by stringResource(R.string.state_date)
    private val responsibleTeam by stringResource(R.string.responsible_team)
    private val delimiter: String = ": "

    private var currentDialog: ProgressDialog? = null
    private var currentRequestDialog: AlertDialog? = null

    override fun init() {
        super.init()
        showContent()
        val requests = databaseController.requests
        if (requests == null || requests.isEmpty())
            showEmpty(R.drawable.ic_assignment, getString(R.string.no_requests), "")
        else {
            val temp = Calendar.getInstance()
            temp.add(Calendar.MONTH, -1)
            val weekBefore = temp.time
            val callback = { request: RequestTitle ->
                val data = databaseController.getRequest(request)
                if (data != null) {
                    showRequestDialog(data)
                } else {
                    loadData(request)
                }
            }
            setAdapter(RequestsAdapter(activity, HeaderAdapter.split(requests, thisMonth, older) { DateFormat.parse(date).before(weekBefore) }, callback))
        }
    }

    private fun showRequestDialog(data: RequestData) {
        dismissDialogs()
        currentRequestDialog = AlertDialog.Builder(activity)
                .setTitle(data.title)
                .setMessage(getDialogText(data))
                .setCancelable(true)
                .setPositiveButton(R.string.confirmation) { a, b -> }
                .setNeutralButton(R.string.copy_request_id) { a, b ->
                    (activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = ClipData.newPlainText("Request ID", data.id)
                    toast(R.string.copied)
                }
                .create() apply { show() }
    }

    private fun getDialogText(data: RequestData): CharSequence {
        return StringBuilder {
            append(requestNumber).append(delimiter).append(data.id).append('\n')
            append(date).append(delimiter).append(data.date).append('\n')
            append(semester).append(delimiter).append(data.semester).append('\n')
            append(course).append(delimiter).append(data.course).append('\n')
            append(requestState).append(delimiter).append(data.state).append('\n')
            append(stateDate).append(delimiter).append(data.stateDate).append('\n')
            append(responsibleTeam).append(delimiter).append(data.responsible).append('\n')

        }
    }

    private fun loadData(request: RequestTitle) {
        dismissDialogs()
        currentDialog = ProgressDialog.show(activity, getString(R.string.refreshing), "")
        apiController.getRequest(databaseController.id, databaseController.session, databaseController.deviceId, ApiController.FakeIp, request) success {
            databaseController.setRequest(it)
        } successUi {
            dismissDialogs()
            showRequestDialog(it)
        } failUi {
            dismissDialogs()
            toast(R.string.refresh_error)
        }
    }

    override fun onPause() {
        super.onPause()
        dismissDialogs()
    }

    private fun dismissDialogs() {
        currentDialog?.dismiss()
        currentDialog = null
        currentRequestDialog?.dismiss()
        currentRequestDialog = null
    }

    companion object {
        private val DateFormat = SimpleDateFormat("dd/MM/yyyy")
    }
}