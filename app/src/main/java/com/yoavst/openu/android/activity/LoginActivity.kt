/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.activity

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.ViewGroup
import com.readystatesoftware.systembartint.SystemBarTintManager
import com.yoavst.openu.R
import com.yoavst.openu.android.getCompatDrawable
import com.yoavst.openu.android.getDeviceId
import com.yoavst.openu.android.isNetworkAvailable
import com.yoavst.openu.android.show
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.controller.DatabaseController
import com.yoavst.openu.controller.RefreshController
import kotlinx.android.synthetic.activity_login.*
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.*
import uy.kohesive.injekt.injectLazy

public class LoginActivity : AppCompatActivity(), RefreshController.Refresher {
    private val requestsController: ApiController by injectLazy()
    private val databaseController: DatabaseController by injectLazy()
    private val refreshController: RefreshController by injectLazy()
    private var isShowingAnimation: Boolean = false
    private var isFirstTimeRefreshing: Boolean = true
    private var hasLogined = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (databaseController.session.isNotEmpty()) {
            setContentView(R.layout.activity_loading)
            if (databaseController.hasEnoughCache()) {
                val button = findViewById(R.id.skip)
                button.show()
                button.setOnClickListener {
                    refreshController.removeCallback(this)
                    launchMainActivity()
                }
            }
            if (isNetworkAvailable) {
                // We need to re-login every time since session expired time is unknown.
                login(databaseController.username, databaseController.password, databaseController.id) successUi {
                    refreshController.refresh(databaseController.id, databaseController.session)
                } failUi {
                    it.printStackTrace()
                    login(databaseController.username, databaseController.password, databaseController.id) successUi {
                        refreshController.refresh(databaseController.id, databaseController.session)
                    } failUi {
                        it.printStackTrace()
                        AlertDialog.Builder(this)
                                .setTitle(R.string.re_login)
                                .setMessage(R.string.do_you_want_to_re_login)
                                .setPositiveButton(R.string.yes) { a, b ->
                                    databaseController.clearData()
                                    recreate()
                                }.setNegativeButton(R.string.no) { a, b ->
                            toast(R.string.refresh_error)
                            finish()
                        }.show()
                    }
                }
            } else {
                toast(R.string.no_connection)
                if (databaseController.hasEnoughCache()) launchMainActivity()
                else finish()
            }
        } else {
            setContentView(R.layout.activity_login)
            loginFab.setImageDrawable(getCompatDrawable(R.drawable.ic_send))
            login.attachListener {
                (login.getChildAt(2) as ViewGroup getChildAt 0).setPadding(0, 0, 0, 0)
                val padding = dip(16)
                (login.getChildAt(2) as ViewGroup getChildAt 0).setPadding(padding, padding, padding, padding)
                Handler().postDelayed({
                    launchMainActivity()
                }, 2000)

            }
            loginFab.setOnClickListener {
                if (!isShowingAnimation) {
                    username.error = null
                    password.error = null
                    id.error = null
                    if (username.length() < 4) username.error = getString(R.string.short_field)
                    else if (id.length() < 9) id.error = getString(R.string.short_field)
                    else if (password.length() < 4) password.error = getString(R.string.short_field)
                    else if (!isNetworkAvailable) toast(R.string.no_connection)
                    else {
                        lockFields()
                        hideKeyboard()
                        showAnimation()
                        login(username.text.toString(), password.text.toString(), id.text.toString()) successUi {
                            hasLogined = true
                            refreshController.refresh(username.text.toString(), it)
                        } failUi {
                            unlockFields()
                            hideAnimation()
                            toast(R.string.error)
                        }
                    }
                }
            }
        }
        setMargin()
        loadBackground()
    }

    override fun refreshed() {
        if (hasLogined) {
            login.beginFinalAnimation()
        } else {
            toast(R.string.refreshed)
            launchMainActivity()
        }
    }

    override fun failedToRefresh() {
        if (hasLogined) {
            toast(R.string.error)
            finish()
        } else if (isFirstTimeRefreshing) {
            isFirstTimeRefreshing = false
            refreshController.refresh(databaseController.id, databaseController.session)
        } else {
            hasLogined = true
            findViewById(R.id.skip)?.isEnabled = false
            login(databaseController.username, databaseController.password, databaseController.id) successUi {
                refreshController.refresh(databaseController.id, databaseController.session)
            } failUi {
                if (databaseController.hasEnoughCache()) {
                    toast(R.string.no_connection)
                    launchMainActivity()
                } else {
                    toast(R.string.error)
                    finish()
                }
            }
        }
    }

    public override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_ENTER -> {
                if (id.isFocused) loginFab.performClick()
                return true
            }
            else -> return super.onKeyUp(keyCode, event)
        }
    }

    override fun onStart() {
        super.onStart()
        refreshController.addCallback(this)
    }

    override fun onStop() {
        super.onStop()
        refreshController.removeCallback(this)
    }

    private fun login(username: String, password: String, id: String): Promise<String, Exception> {
        val deviceId = getDeviceId()
        return requestsController.login(id, username, password, deviceId, ApiController.FakeIp) success {
            databaseController.bulk {
                this.id = id
                this.username = username
                this.password = password
                this.session = it
                this.deviceId = deviceId
            }
        }
    }

    private fun lockFields() {
        id.isEnabled = false
        password.isEnabled = false
        username.isEnabled = false
    }

    private fun unlockFields() {
        id.isEnabled = true
        password.isEnabled = true
        username.isEnabled = true
    }

    private fun setMargin() {
        val navBarHeight = SystemBarTintManager(this).config.navigationBarHeight
        findViewById(R.id.background).setPadding(0, navBarHeight, 0, navBarHeight)
    }


    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun showAnimation() {
        login.show()
        isShowingAnimation = true
    }

    private fun hideAnimation() {
        login.hide()
        isShowingAnimation = false
    }

    private fun loadBackground() {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val bmOptions = BitmapFactory.Options()
        val scaleFactor = Math.min(1080 / size.x, 1920 / size.y)
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true
        bmOptions.inPreferredConfig = Bitmap.Config.RGB_565
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.splash, bmOptions)
        findViewById(R.id.background).background = BitmapDrawable(resources, bitmap)
    }

    private fun launchMainActivity() {
        startActivity<MainActivity>()
        finish()
    }
}