/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.activity

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.TabsAdapter
import com.yoavst.openu.android.getCompatDrawable
import com.yoavst.openu.android.isNetworkAvailable
import com.yoavst.openu.android.views.AppBarLayoutBehavior
import com.yoavst.openu.controller.DatabaseController
import com.yoavst.openu.controller.RefreshController
import kotlinx.android.synthetic.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import uy.kohesive.injekt.injectLazy

public class MainActivity : AppCompatActivity(), RefreshController.Refresher {
    private val refreshController: RefreshController by injectLazy()
    private val databaseController: DatabaseController by injectLazy()
    private var isRefreshing = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        pager.adapter = TabsAdapter(fragmentManager)
        pager.offscreenPageLimit = 4
        tabs.setupWithViewPager(pager)
        for (i in 0..tabs.tabCount - 1) {
            tabs.getTabAt(i).setIcon(getCompatDrawable(TabsAdapter.TabsIcons[i]))
        }
        pager.currentItem = tabs.tabCount - 1
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.refresh -> {
                    if (isNetworkAvailable) {
                        if (!isRefreshing) {
                            refreshController.refresh(databaseController.id, databaseController.session)
                        }
                    } else Snackbar.make(coordinatorLayout, R.string.no_connection, Snackbar.LENGTH_SHORT).show()
                }
                R.id.logout -> {
                    databaseController.clearData()
                    finish()
                    startActivity<LoginActivity>()
                }
            }
            true
        }
        nextView.setImageDrawable(getCompatDrawable(R.drawable.ic_keyboard_arrow_left_white))
        lastView.setImageDrawable(getCompatDrawable(R.drawable.ic_keyboard_arrow_right_white))
        (appBar.layoutParams as CoordinatorLayout.LayoutParams).behavior = AppBarLayoutBehavior()

    }

    override fun refreshed() {
        isRefreshing = false
        toast(R.string.refreshed)
        pager.adapter.notifyDataSetChanged()
    }

    override fun failedToRefresh() {
        isRefreshing = false
        Snackbar.make(coordinatorLayout, R.string.refresh_error, Snackbar.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        refreshController.removeCallback(this)
    }

    override fun onResume() {
        super.onResume()
        refreshController.addCallback(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)
        menu.getItem(0).setIcon(getCompatDrawable(R.drawable.ic_refresh))
        return true
    }
}