/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.app.ProgressDialog
import android.support.v7.app.AlertDialog
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.widget.TextView
import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.HeaderAdapter
import com.yoavst.openu.android.adapters.MessagesAdapter
import com.yoavst.openu.android.stringResource
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.model.MessageData
import com.yoavst.openu.model.MessageTitle
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.Calendar


public class MessagesFragment : BaseListFragment() {
    private val thisWeek by stringResource(R.string.this_week)
    private val older by stringResource(R.string.older)

    private val date by stringResource(R.string.date)
    private val smsMessage by stringResource(R.string.sms_message)
    private val mailMessage by stringResource(R.string.mail_message)
    private val links by stringResource(R.string.links)

    private val delimiter: String = ": "

    private var currentDialog: ProgressDialog? = null
    private var currentRequestDialog: AlertDialog? = null

    override fun init() {
        super.init()
        showContent()
        val messages = databaseController.messages
        if (messages == null || messages.isEmpty())
            showEmpty(R.drawable.ic_message, getString(R.string.no_messages), "")
        else {

            val callback = { message: MessageTitle ->
                val data = databaseController.getMessageData(message)
                if (data != null) {
                    showMessageDialog(data)
                } else {
                    loadData(message)
                }
            }
            val temp = Calendar.getInstance()
            temp.add(Calendar.DAY_OF_WEEK, -7)
            val weekBefore = temp.time
            setAdapter(MessagesAdapter(activity, HeaderAdapter.split(messages, thisWeek, older) { DateFormat.parse(date).before(weekBefore) }, callback))
        }
    }

    private fun showMessageDialog(message: MessageData) {
        dismissDialogs()
        currentRequestDialog = AlertDialog.Builder(activity)
                .setTitle(message.title)
                .setMessage(getMessageText(message))
                .setPositiveButton(android.R.string.ok) { a, b ->

                }
                .create() apply { show() }
        (currentRequestDialog!!.findViewById(android.R.id.message) as TextView).movementMethod = LinkMovementMethod.getInstance();
    }

    private fun getMessageText(message: MessageData): CharSequence {
        return SpannableStringBuilder() apply {
            append(date).append(delimiter).append(message.date).append('\n')
            if (message.smsMessage.trim().isNotEmpty())
                append(smsMessage).append(delimiter).append(message.smsMessage).append('\n')
            if (message.mailMessage.trim().isNotEmpty())
                append(mailMessage).append(delimiter).append(message.mailMessage).append('\n')
            if (message.links.isNotEmpty()) {
                append(links).append(delimiter).append('\n')
                message.links.forEach {
                    append(it.first, URLSpan(it.second), 0).append("\n")
                }
            }
        }
    }

    private fun loadData(message: MessageTitle) {
        dismissDialogs()
        currentDialog = ProgressDialog.show(activity, getString(R.string.refreshing), "")
        apiController.getMessage(databaseController.id, databaseController.session, databaseController.deviceId, ApiController.FakeIp, message) success {
            databaseController.setMessageDate(message, it)
        } successUi {
            dismissDialogs()
            showMessageDialog(it)
        } failUi {
            dismissDialogs()
            toast(R.string.refresh_error)
        }
    }

    override fun onPause() {
        super.onPause()
        dismissDialogs()
    }

    private fun dismissDialogs() {
        currentDialog?.dismiss()
        currentDialog = null
        currentRequestDialog?.dismiss()
        currentRequestDialog = null
    }

    companion object {
        private val DateFormat = SimpleDateFormat("dd/MM/yyyy")
    }
}