/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.activity

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.CourseTabsAdapter
import com.yoavst.openu.android.getCompatDrawable
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.controller.DatabaseController
import com.yoavst.openu.model.CourseData
import com.yoavst.openu.model.CourseTitle
import com.yoavst.openu.model.Provider
import kotlinx.android.synthetic.activity_course.*
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import uy.kohesive.injekt.injectLazy

public class CourseActivity : AppCompatActivity(), Provider<CourseData> {
    val databaseController: DatabaseController by injectLazy()
    val apiController: ApiController by injectLazy()
    var snackbar: Snackbar? = null
    var progressDialog: ProgressDialog? = null
    val data: CourseData by lazy { databaseController.getCourseData(intent.getStringExtra(Extra_Id))!! }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course)
        back.setOnClickListener { finish() }
        initPager()
        title.text = data.title.name
        courseNumber.text = getString(R.string.course_number) + " " + data.title.id
        courseSemester.text = getString(R.string.semester) + ": " + data.title.term

        if (!intent.getBooleanExtra(Extra_IsDataNew, false)) {
            snackbar = Snackbar.make(coordinatorLayout, R.string.should_update_this, Snackbar.LENGTH_INDEFINITE).setAction(R.string.refresh) {
                progressDialog = ProgressDialog.show(this, getString(R.string.refreshing), "")
                apiController.getCourseData(databaseController.id, databaseController.session, databaseController.deviceId, ApiController.FakeIp, data.title) success {
                    databaseController.setCourseData(it)
                } successUi {
                    snackbar?.dismiss()
                    snackbar = null
                    hideDialogs()
                    pager.adapter.notifyDataSetChanged()
                } failUi {
                    toast(R.string.refresh_error)
                }
            } apply { show() }
        }
    }

    private fun initPager() {
        pager.adapter = CourseTabsAdapter(fragmentManager)
        pager.offscreenPageLimit = 4
        tabs.setupWithViewPager(pager)
        for (i in 0..tabs.tabCount - 1) {
            tabs.getTabAt(i).setIcon(getCompatDrawable(CourseTabsAdapter.TabsIcons[i]))
        }
        pager.currentItem = tabs.tabCount - 1
    }

    override fun onPause() {
        super.onPause()
        hideDialogs()
    }

    private fun hideDialogs() {
        progressDialog?.dismiss()
        progressDialog = null
    }

    override fun get(): CourseData = data

    companion object {
        private val Extra_IsDataNew: String = "cache"
        private val Extra_Id: String = "id"

        public fun start(context: Activity, title: CourseTitle, isDataNew: Boolean = false) {
            context.startActivity<CourseActivity>(Extra_IsDataNew to isDataNew, Extra_Id to title.id)
        }
    }
}