/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.support.design.widget.Snackbar
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Space
import android.widget.TextView
import com.yoavst.openu.R
import kotlinx.android.synthetic.fragment_course_details.fullLayout
import kotlinx.android.synthetic.fragment_course_details.layout
import org.jetbrains.anko.dip
import org.jetbrains.anko.toast


public class CourseInfoFragment : BaseFragment(), CourseFragment {
    override val layoutId: Int = R.layout.fragment_course_details
    private val marginSize: Int by lazy { dip(8) }
    private val spaceSize: Int by lazy { marginSize }
    private val backgroundId: Int by lazy {
        val typedArray = activity.obtainStyledAttributes(intArrayOf(R.attr.selectableItemBackground))
        val backgroundResource = typedArray.getResourceId(0, 0)
        typedArray.recycle()
        backgroundResource
    }
    private var oldSnackbar: Snackbar? = null

    override fun init() {
        val details = data.details
        val phone = s(R.string.phone)
        val supportPhone = s(R.string.phone_supporting)
        layout.text(s(R.string.course_status), details.status)
        space()
        group {
            call(details.teachingCenterPhone)
        } apply {
            title(s(R.string.learning_center), details.teachingCenter)
            text(phone, details.teachingCenterPhone)
            text(supportPhone, details.phoneTeaching)
        }
        group {
            call(details.teacherPhone)
        } apply {
            title(s(R.string.secretariat), details.secretariat)
            text(phone, details.secretariatPhone)
            text(s(R.string.clazz), details.secretariatClass)
        }
        group {
            call(details.teacherPhone)
        } apply {
            title(s(R.string.teacher), details.teacher)
            text(phone, details.teacherPhone)
            text(supportPhone, details.teacherPhoneTeaching)
        }
        space()
        layout.text(s(R.string.address_for_sending), details.addressForSending) setOnClickListener {
            (activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = ClipData.newPlainText("Sending Address", details.addressForSending)
            toast(R.string.address_copied)
        }
    }

    private fun call(number: String) {
        oldSnackbar?.dismiss()
        oldSnackbar = Snackbar.make(fullLayout, R.string.do_you_want_call, Snackbar.LENGTH_SHORT).setAction(R.string.call) {
            startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number")))
        } apply { show() }
    }

    private fun s(id: Int) = getString(id)

    private fun ViewGroup.title(name: String, value: String): View {
        return textView(name + ": " + value, 18.0f, true)
    }

    private fun ViewGroup.text(name: String, value: String): View {
        return textView(name + ": " + value, 16.0f)
    }

    private fun space(): View {
        val space = Space(activity)
        space.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, spaceSize)
        layout.addView(space)
        return space
    }

    private fun ViewGroup.textView(text: String, fontSize: Float, bold: Boolean = false): View {
        val textView = TextView(activity)
        textView.text = text
        textView.textSize = fontSize
        if (bold) textView.setTypeface(textView.typeface, Typeface.BOLD)
        val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        params.setMargins(0, 0, 0, marginSize)
        textView.layoutParams = params
        addView(textView)
        return textView
    }

    private fun group(listener: (View) -> Unit): ViewGroup {
        val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val viewGroup = LinearLayout(activity)
        viewGroup.layoutParams = params
        viewGroup.orientation = LinearLayout.VERTICAL
        viewGroup.setBackgroundResource(backgroundId)
        viewGroup.setOnClickListener(listener)
        layout.addView(viewGroup)
        return viewGroup
    }
}