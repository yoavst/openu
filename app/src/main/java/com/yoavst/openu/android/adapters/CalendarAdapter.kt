/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.openu.R
import com.yoavst.openu.android.hide
import com.yoavst.openu.model.CalendarTitle
import com.yoavst.openu.model.VisibleItem

public class CalendarAdapter(context: Context, data: Array<VisibleItem<CalendarTitle>>, callback: (CalendarTitle) -> Unit) : HeaderAdapter<CalendarTitle>(context, data, callback) {
    val day = context.getString(R.string.day)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: CalendarTitle) {
        holder as HeaderAdapter.BaseItemViewHolder
        holder.title.text = item.title
        holder.arrow.hide()
        if (holder is HeaderAdapter.ExtendedItemViewHolder) {
            holder.extra.text = item.location
            holder.extraUp.text = item.hours
        } else holder.extra.text = item.hours
    }


    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TypeOneLine) return HeaderAdapter.BaseItemViewHolder(inflater.inflate(R.layout.item_1_line_extra, parent, false))
        else return HeaderAdapter.ExtendedItemViewHolder(inflater.inflate(R.layout.item_2_lines_extra, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        if (items[position].title == null) {
            if (items[position].data!!.location.isEmpty()) return TypeOneLine
            else return HeaderAdapter.TypeData
        } else return HeaderAdapter.TypeHeader
    }

    override fun onCreateHeaderViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        return HeaderViewHolder(inflater.inflate(R.layout.item_calendar_header, parent, false))
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder, title: String) {
        (holder as HeaderViewHolder).apply {
            val parts = title.split('|')
            dayOfWeek.text = day + " " + parts[0]
            date.text = parts[1]
        }
    }

    public class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val dayOfWeek: TextView = itemView.findViewById(R.id.dayOfWeek) as TextView
        public val date: TextView = itemView.findViewById(R.id.date) as TextView

    }

    companion object {
        private val TypeOneLine = 10
    }

}