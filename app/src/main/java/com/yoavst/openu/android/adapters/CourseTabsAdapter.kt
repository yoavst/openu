/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.adapters

import android.app.Fragment
import android.app.FragmentManager
import android.support.v13.app.FragmentPagerAdapter
import com.yoavst.openu.R
import com.yoavst.openu.android.fragments.CourseGradesFragment
import com.yoavst.openu.android.fragments.CourseInfoFragment
import com.yoavst.openu.android.fragments.CourseMeetingsFragment
import com.yoavst.openu.android.fragments.CoursePaymentsFragment


public class CourseTabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> CoursePaymentsFragment()
            1 -> CourseGradesFragment()
            2 -> CourseMeetingsFragment()
            3 -> CourseInfoFragment()
            else -> throw IllegalArgumentException()
        }
    }

    override fun getCount(): Int = 4

    companion object {
        public val TabsIcons: IntArray = intArrayOf(R.drawable.ic_payment, R.drawable.ic_school, R.drawable.ic_event, R.drawable.ic_info)
    }
}