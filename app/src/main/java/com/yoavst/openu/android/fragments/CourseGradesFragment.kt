/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.CourseGradesAdapter
import com.yoavst.openu.model.CourseGrade
import com.yoavst.openu.model.VisibleItem


public class CourseGradesFragment : BaseListFragment(), CourseFragment {
    override fun init() {
        super.init()
        showContent()
        val grades = data.grades
        if (grades.isEmpty())
            showEmpty(R.drawable.ic_school, getString(R.string.no_grades), "")
        else {
            val callback = { grade: CourseGrade ->

            }
            setAdapter(CourseGradesAdapter(activity, (grades map { VisibleItem(it) }).toTypedArray(), callback))
        }
    }
}