/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import com.yoavst.openu.R
import com.yoavst.openu.android.adapters.CourseMeetingsAdapter
import com.yoavst.openu.android.adapters.HeaderAdapter
import com.yoavst.openu.model.CourseMeeting
import java.text.SimpleDateFormat
import java.util.*

public class CourseMeetingsFragment: BaseListFragment(), CourseFragment {
    override fun init() {
        super.init()
        showContent()
        val meetings = data.meetings
        if (meetings.isEmpty())
            showEmpty(R.drawable.ic_event, getString(R.string.no_meetings), "")
        else {
            val callback = { grade: CourseMeeting ->

            }
            val regex = "[0-9]{2}\\.[0-9]{2}\\.[0-9]{2}".toRegex()
            val format = SimpleDateFormat("dd.MM.yy")
            val now = Date()
            setAdapter(CourseMeetingsAdapter(activity, HeaderAdapter.split(meetings, getString(R.string.meetings_ended), getString(R.string.meetings_future)) {
                format.parse(regex.match(date)!!.value).after(now)
            }, callback))
        }
    }
}