/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.app.ProgressDialog
import com.yoavst.openu.R
import com.yoavst.openu.android.activity.CourseActivity
import com.yoavst.openu.android.adapters.CoursesAdapter
import com.yoavst.openu.android.adapters.HeaderAdapter
import com.yoavst.openu.android.stringArrayResource
import com.yoavst.openu.android.stringResource
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.model.CourseTitle
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

public class CoursesFragment : BaseListFragment() {
    private val simesters by stringArrayResource(R.array.end_dates)
    val inLearning by stringResource(R.string.in_learning)
    val finished by stringResource(R.string.finished)

    private var loadingDialog: ProgressDialog? = null

    override fun init() {
        super.init()
        showContent()
        val courses = databaseController.courses
        if (courses == null || courses.isEmpty())
            showEmpty(R.drawable.ic_class, getString(R.string.no_courses), "")
        else {
            val callback = { course: CourseTitle ->
                if (databaseController.hasCourseData(course))
                    CourseActivity.start(activity, course)
                else {
                    dismissDialog()
                    loadingDialog = ProgressDialog.show(activity, getString(R.string.refreshing), "")
                    apiController.getCourseData(databaseController.id, databaseController.session, databaseController.deviceId, ApiController.FakeIp, course) success {
                        databaseController.setCourseData(it)
                    } successUi {
                        dismissDialog()
                        CourseActivity.start(activity, course, true)
                    } failUi {
                        dismissDialog()
                        toast(R.string.refresh_error)
                    }
                }
                Unit
            }
            val now = Date()
            setAdapter(CoursesAdapter(activity, HeaderAdapter.split(courses, inLearning, finished) {
                if (term.substring(0, 4).toInt() < 2015) true
                else {
                    for (i in 0..(simesters.size() - 1) step 2) {
                        if (simesters[i] == term) return@split DateFormat.parse(simesters[i + 1]).before(now)
                    }
                    false
                }
            }, callback))
        }
    }

    override fun onPause() {
        super.onPause()
        dismissDialog()
    }

    private fun dismissDialog() {
        loadingDialog?.dismiss()
        loadingDialog = null
    }

    companion object {
        private val DateFormat = SimpleDateFormat("dd.MM.yyyy")
    }
}