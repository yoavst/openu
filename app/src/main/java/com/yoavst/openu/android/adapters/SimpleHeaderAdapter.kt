/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.openu.R
import com.yoavst.openu.model.VisibleItem

public abstract class SimpleHeaderAdapter<K : Any>(context: Context, items: Array<VisibleItem<K>>, callback: (K) -> Unit) : HeaderAdapter<K>(context, items, callback) {
    protected abstract val layout: Int

    public class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val title: TextView = itemView as TextView
    }

    override fun onCreateHeaderViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        return HeaderViewHolder(inflater.inflate(R.layout.item_header, parent, false))
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder, title: String) {
        (holder as HeaderViewHolder).title.text = title
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return onCreateViewHolder(inflater.inflate(layout, parent, false))
    }

    protected abstract fun onCreateViewHolder(layout: View): RecyclerView.ViewHolder

}