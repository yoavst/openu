/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import com.wnafee.vector.compat.ResourcesCompat
import org.jetbrains.anko.connectivityManager
import org.jetbrains.anko.telephonyManager
import java.net.URLDecoder
import java.util.*

public val Context.isNetworkAvailable: Boolean
    get() = connectivityManager.activeNetworkInfo != null

public fun Context.getCompatDrawable(id: Int): Drawable = ResourcesCompat.getDrawable(this, id)

public fun Calendar.clean(): Calendar {
    set(Calendar.HOUR_OF_DAY, 0)
    set(Calendar.MINUTE, 0)
    set(Calendar.SECOND, 0)
    set(Calendar.MILLISECOND, 0)
    return this
}

public fun Date.toCalendar(): Calendar {
    val cal = Calendar.getInstance()
    cal.time = this
    cal.clean()
    return cal
}

public fun Context.getDeviceId(): String = telephonyManager.deviceId

public fun getQueryParams(url: String): Map<String, List<String>> {
    val params = HashMap<String, MutableList<String>>()
    val urlParts = url.split('?')
    if (urlParts.size() > 1) {
        val query = urlParts[1]
        for (param in query.split('&')) {
            val pair = param.split('=')
            val key = URLDecoder.decode(pair[0], "UTF-8")
            var value = ""
            if (pair.size() > 1) {
                value = URLDecoder.decode(pair[1], "UTF-8")
            }

            var values: MutableList<String>? = params[key]
            if (values == null) {
                values = ArrayList<String>()
                params[key] = values
            }
            values.add(value)
        }
    }
    return params
}

public fun View.show(): Unit {
    visibility = View.VISIBLE
}

public fun View.hide(): Unit {
    visibility = View.GONE
}
