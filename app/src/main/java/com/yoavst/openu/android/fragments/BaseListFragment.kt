/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.fragments

import android.support.v7.widget.RecyclerView
import com.yoavst.openu.R
import com.yoavst.openu.android.getCompatDrawable
import com.yoavst.openu.controller.ApiController
import com.yoavst.openu.controller.DatabaseController
import kotlinx.android.synthetic.fragment_list.list
import kotlinx.android.synthetic.fragment_list.progressActivity
import org.solovyev.android.views.llm.LinearLayoutManager
import uy.kohesive.injekt.injectLazy

public abstract class BaseListFragment : BaseFragment() {
    protected val databaseController: DatabaseController by injectLazy()
    protected val apiController: ApiController by injectLazy()
    override val layoutId: Int = R.layout.fragment_list

    override fun init() {
        list.layoutManager = LinearLayoutManager(activity)
    }

    protected fun showEmpty(drawable: Int, title: String, content: String) {
        progressActivity.showEmpty(activity.getCompatDrawable(drawable), title, content)
    }

    protected fun showContent() {
        progressActivity.showContent()
    }

    protected fun setAdapter(adapter: RecyclerView.Adapter<*>) {
        list.adapter = adapter
    }


}