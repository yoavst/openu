/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.yoavst.openu.R
import com.yoavst.openu.android.getCompatDrawable
import com.yoavst.openu.model.CourseGrade
import com.yoavst.openu.model.VisibleItem

public class CourseGradesAdapter(context: Context, data: Array<VisibleItem<CourseGrade>>, callback: (CourseGrade) -> Unit) : SimpleHeaderAdapter<CourseGrade>(context, data, callback) {
    val arrow = context.getCompatDrawable(R.drawable.ic_keyboard_arrow_left)

    override val layout: Int = R.layout.item_2_lines_centered_extra

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: CourseGrade) {
        holder as HeaderAdapter.ExtendedItemViewHolder
        holder.arrow.setImageDrawable(arrow)
        holder.title.text = item.type + " " + item.id
        holder.extra.text = item.date
        holder.extraUp.text = item.grade
    }

    override fun onCreateViewHolder(layout: View): RecyclerView.ViewHolder = HeaderAdapter.ExtendedItemViewHolder(layout)

}