/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.android.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.yoavst.openu.R
import com.yoavst.openu.model.VisibleItem

public abstract class HeaderAdapter<K : Any>(context: Context, protected val items: Array<VisibleItem<K>>, private val callback: (K) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TypeHeader) onBindHeaderViewHolder(holder, items[position].title!!)
        else {
            holder.itemView.setOnClickListener { callback(items[position].data!!) }
            onBindViewHolder(holder, items[position].data!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TypeHeader) return onCreateHeaderViewHolder(inflater, parent)
        else return onCreateViewHolder(inflater, parent, viewType)
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].title == null) TypeData else TypeHeader
    }

    override fun getItemCount(): Int = items.size()


    public open class BaseItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val title: TextView by lazy { this.itemView.findViewById(R.id.title) as TextView }
        public val extra: TextView by lazy { this.itemView.findViewById(R.id.extra) as TextView }
        public val arrow: ImageView by lazy { this.itemView.findViewById(R.id.arrow) as ImageView }
    }

    public open class ExtendedItemViewHolder(view: View) : HeaderAdapter.BaseItemViewHolder(view) {
        val extraUp: TextView by lazy { itemView.findViewById(R.id.extraUp) as TextView }
    }


    protected abstract fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: K)
    protected abstract fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
    protected abstract fun onCreateHeaderViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder
    protected abstract fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder, title: String)


    companion object {
        protected val TypeHeader: Int = 1
        protected val TypeData: Int = 0

        public inline fun split<K: Any>(data: Array<K>, firstTitle: String, secondTitle: String, rule: K.() -> Boolean): Array<VisibleItem<K>> {
            var oldPosition = -1
            for (i in 0..data.size() - 1) {
                if (data[i].rule()) {
                    oldPosition = i
                    break
                }
            }
            if (oldPosition == -1) {
                return Array(data.size() + 1) {
                    if (it == 0) VisibleItem<K>(title = firstTitle)
                    else VisibleItem(data[it - 1])
                }
            } else if (oldPosition == 0) {
                return Array(data.size() + 1) {
                    if (it == 0) VisibleItem<K>(title = secondTitle)
                    else VisibleItem(data[it - 1])
                }
            } else {
                return Array(data.size() + 2) {
                    if (it == 0) VisibleItem<K>(title = firstTitle)
                    else if (it == oldPosition + 1) VisibleItem<K>(title = secondTitle)
                    else if (it < oldPosition + 1) VisibleItem(data[it - 1])
                    else VisibleItem(data[it - 2])
                }
            }
        }
    }
}