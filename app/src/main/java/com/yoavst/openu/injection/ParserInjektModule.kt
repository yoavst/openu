/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.injection

import com.yoavst.openu.model.*
import com.yoavst.openu.parser.*
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.fullType

public object ParserInjektModule : InjektModule {
    override fun InjektRegistrar.registerInjectables() {
        addSingleton(fullType<Parser<CalendarEventExtra>>(), CalendarEventParser)
        addSingleton(fullType<Parser<Session>>(), LoginParser)
        addSingleton(fullType<Parser<CourseDetails>>(), CourseDetailsParser)
        addSingleton(fullType<Parser<Array<CourseTitle>>>(), CoursesParser)
        addSingleton(fullType<Parser<MessageData>>(), MessageParser)
        addSingleton(fullType<Parser<Array<MessageTitle>>>(), MessagesParser)
        addSingleton(fullType<Parser<RequestData>>(), RequestParser)
        addSingleton(fullType<Parser<Array<RequestTitle>>>(), RequestsParser)
        addSingleton(fullType<Parser<CalendarWeek>>(), WeeklyCalendarParser)
        addSingleton(fullType<Parser<Array<CoursePayment>>>(), CoursePaymentsParser)
        addSingleton(fullType<Parser<Array<CourseGrade>>>(), CourseGradesParser)
        addSingleton(fullType<Parser<Array<CourseMeeting>>>(), CourseMeetingsParser)
    }
}