/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.injection

import com.yoavst.openu.controller.*
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.fullType

public object ControllersInjektModule : InjektModule {
    override fun InjektRegistrar.registerInjectables() {
        addSingleton(fullType<RequestsController>(), RequestsControllerImpl)
        addSingleton(fullType<ApiController>(), ApiControllerImpl)
        addSingleton(fullType<DatabaseController>(), DatabaseControllerImpl)
        addSingleton(fullType<RefreshController>(), RefreshControllerImpl)
    }
}