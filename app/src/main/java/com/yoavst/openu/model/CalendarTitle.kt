/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.model

import java.text.SimpleDateFormat

public data class CalendarTitle(public val title: String, public val date: String, public val hours: String, public val location: String, public val id: String) {
    companion object {
        public val DateFormat: SimpleDateFormat = SimpleDateFormat("dd.MM.yy")
        public val ExtraData_DateFormat: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
    }
}