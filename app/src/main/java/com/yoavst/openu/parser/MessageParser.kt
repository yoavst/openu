/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.model.MessageData
import org.jsoup.nodes.Document
import java.util.HashMap

/**
 * Created by yoavst.
 */
public object MessageParser : Parser<MessageData> {
    override fun parse(document: Document): MessageData {
        var date = document.select(".DateStyle").first().ownText()
        val title = document.getElementById("MessageTitle").ownText()
        date = "[0-9]{2}\\.[0-9]{2}\\.[0-9]{2}".toRegex().match(date)!!.value
        val smsMessage = document.getElementsByClass("MessageSMSContent").firstOrNull()?.ownText().orEmpty()
        val mailMessage = document.getElementsByClass("ContentStyle").not("#MessageSMSContent").firstOrNull()?.text().orEmpty()
        return MessageData(title, date, smsMessage, mailMessage, (document.getElementsByTag("a") map { it.ownText() to it.attr("href")}).toTypedArray())
    }
}