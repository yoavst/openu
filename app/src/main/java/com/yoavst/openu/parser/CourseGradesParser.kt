/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.model.CourseGrade
import org.jsoup.nodes.Document

public object CourseGradesParser : Parser<Array<CourseGrade>> {
    override fun parse(document: Document): Array<CourseGrade> {
        val elements = document.getElementsByTag("a")
        return Array(elements.size()) {
            val a = elements[it]
            CourseGrade(id = a.getElementsByClass("clm1").first().ownText(), type = a.getElementsByClass("clm2").first().ownText(),
                    grade = a.getElementsByClass("clm3").first().ownText(), date = a.getElementsByClass("clm4").first().ownText())
        }
    }
}