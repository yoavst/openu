/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.android.getQueryParams
import com.yoavst.openu.model.CourseMeeting
import org.jsoup.nodes.Document

public object CourseMeetingsParser : Parser<Array<CourseMeeting>> {
    override fun parse(document: Document): Array<CourseMeeting> {
        val elements = document.select("a li")
        return Array(elements.size()) {
            val a = elements[it]
            CourseMeeting(number = a.getElementsByClass("firstCol").first().ownText(), date = a.getElementsByClass("secondCol").first().ownText(),
                    hours = a.getElementsByClass("thirdCol").first().ownText(), meetingId = getQueryParams(a.parent().attr("href"))["mID"]!!.first())
        }
    }
}