/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.android.getQueryParams
import com.yoavst.openu.model.RequestTitle
import org.jsoup.nodes.Document

public object RequestsParser : Parser<Array<RequestTitle>> {
    override fun parse(document: Document): Array<RequestTitle> {
        val elements = document.select("a")
        return Array(elements.size()) {
            val div = elements[it].getElementsByTag("div")[0].getElementsByTag("div")
            RequestTitle(title = div[0].allElements[2].ownText(), courseId = div[0].allElements[3].ownText(), id = getId(elements[it].attr("href")),
                    date = div[5].ownText(), status = div[6].allElements.last().ownText())
        }
    }

    private fun getId(url: String): String = getQueryParams(url)["requestID"]!![0]
}