/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.android.getQueryParams
import com.yoavst.openu.model.CalendarTitle
import com.yoavst.openu.model.CalendarWeek
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.util.ArrayList

public object WeeklyCalendarParser : Parser<CalendarWeek> {
    override fun parse(document: Document): CalendarWeek {
        val dates = document.select(".weeklyTblWrap h2 div").first().ownText().split('-')
        val weekNumber = "-?\\d+".toRegex().match(document.select(".weeklyTblWrap h2 span").first().ownText())!!.value.toInt()
        val elements = document.getElementsByTag("div")
        val links = document.getElementsByTag("a")
        var lastData: String = ""
        var lastDate: String = ""
        var element: Element
        val events = ArrayList<CalendarTitle>(elements.size() / 2) // Max elements is (elements.size() - 1) / 2
        for (i in 1..elements.size() - 1) {
            element = elements[i]
            when (element.className()) {
                "weekDayRow" -> {
                    lastDate = "[0-9]{2}\\.[0-9]{2}\\.[0-9]{2}".toRegex().match(element.text())!!.value
                }
                "eventInterval" -> {
                    lastData = element.text()
                }
                "eventDetails" -> {
                    val link = links[events.size() + 2].attr("href")
                    val children = element.children()
                    events.add(CalendarTitle(title = children[0].ownText(), date = lastDate, hours = lastData, location = children[1].ownText(), id = getId(link)))
                }
            }
        }
        return CalendarWeek(weekNumber, dates[0].substring(1), dates[1].substring(0, dates[1].length() - 1), events.toTypedArray())
    }

    private fun getId(url: String): String = getQueryParams(url)["eid"]!![0]

}