/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.model.CourseDetails
import org.jsoup.nodes.Document

public object CourseDetailsParser : Parser<CourseDetails> {
    override fun parse(document: Document): CourseDetails {
        val doc = document.getElementsByTag("ul").first()
        val names = doc.select("a .first + span") map { it.ownText() }
        val data = doc.getElementsByClass("special") map { it.text() }
        val phoneData = doc.select(".special + span + span") map { it.ownText() }
        val address = doc.select("#DetailsAddressMaman div span").first().ownText()
        val status = document.select(".title + h1 div").first().ownText()
        val link = document.select(".blueBoxTitle a").attr("href")
        return CourseDetails(status, teachingCenter = names[0], teachingCenterPhone = data[0], phoneTeaching = phoneData[0], secretariat = names[1],
                secretariatPhone = data[1], secretariatClass = data[2], teacher = names[2], teacherPhone = data[3], teacherPhoneTeaching = phoneData[1],
                addressForSending = address, courseSite = link)
    }

}