/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.model.MessageTitle
import org.jsoup.nodes.Document

public object MessagesParser : Parser<Array<MessageTitle>> {
    override fun parse(document: Document): Array<MessageTitle> {
        val elements = document.select(".MsgSection a")
        val size = elements.size()
        return Array(size) {
            val element = elements[it].allElements
            val link = element.attr("href")
            MessageTitle(element[1].ownText(), element[2].ownText(), link.toNormalAddress(), size - it)
        }
    }
}