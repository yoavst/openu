/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.parser

import com.yoavst.openu.model.CourseTitle
import com.yoavst.openu.parser.Parser
import org.jsoup.nodes.Document


public object CoursesParser : Parser<Array<CourseTitle>> {
    override fun parse(document: Document): Array<CourseTitle> {
        val elements = document.select(".Courses_List li a")
        return Array(elements.size()) {
            val element = elements[it]
            val spans = element.getElementsByTag("span")
            val termAndId = spans[1].ownText()
            val term = termAndId.substring(termAndId.lastIndexOf('\u00a0'), termAndId.length()).substring(8)
            val id = termAndId.substring(0, termAndId.indexOf('\u00a0')).substring(11)
            CourseTitle(spans[0].ownText(), id, term, element.attr("href"))
        }
    }
}