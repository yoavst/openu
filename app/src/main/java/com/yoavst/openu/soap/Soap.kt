/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.soap

/**
 * <?xml version="1.0" encoding="utf-8"?>
 * <Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
 * <Body>
 * $body
 * </Body>
 * </Envelope>
 */
public class Soap() : TagWithText("Envelope") {
    var header: String = """<?xml version="1.0" encoding="utf-8"?>"""

    val body: TagWithText

    init {
        attr("xmlns", "http://schemas.xmlsoap.org/soap/envelope/")
        body = tag("Body") {}
    }

    override fun toString(): String {
        return wrap(super.toString())
    }

    fun wrap(xml: String): String = header + "\n" + xml
}