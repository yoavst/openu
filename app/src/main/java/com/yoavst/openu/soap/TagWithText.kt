/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.soap

import com.yoavst.openu.soap.Tag

open class TagWithText(name: String) : Tag(name) {
    fun String.plus() {
        children.add(TextElement(this))
    }

    fun init(xmlns: String?): TagWithText {
        this.xmlns = xmlns
        return this

    }

    open fun tag(name: String, xmlns: String? = null, init: TagWithText.() -> Unit): TagWithText {
        return initTag(TagWithText(name).init(xmlns), init)
    }

    open fun tag(name: String, value: String? = null, xmlns: String? = null): TagWithText {
        val tag = initTag(TagWithText(name).init(xmlns), {})
        tag.children.add(TextElement(value))
        return tag
    }

    fun attr(key: String, value: String) = attributes.add(Pair(key, value))

}