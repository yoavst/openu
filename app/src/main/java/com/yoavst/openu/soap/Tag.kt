/*
 *     This file is part of OpenU.
 *
 *     OpenU is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenU is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenU.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.yoavst.openu.soap

import java.util.ArrayList

open class Tag(val name: String) : Element {
    val children: ArrayList<Element> = ArrayList()
    val attributes = ArrayList<Pair<String, String>>()
    var xmlns: String? = null

    public fun initTag<T : Element>(tag: T, init: T.() -> Unit): T {
        tag.init()
        children.add(tag)
        return tag
    }

    override fun render(builder: StringBuilder, indent: String) {
        if (children.size() > 0) {
            if (children.size() == 1 && children[0] is TextElement) {
                builder.append("$indent<${renderName()}${renderAttributes()}>")
                children[0].render(builder, "")
                builder.append("</${renderName()}>\n")
            } else {
                builder.append("$indent<${renderName()}${renderAttributes()}>\n")
                for (c in children) {
                    c.render(builder, indent + "  ")
                }
                builder.append("$indent</${renderName()}>\n")
            }
        } else {
            builder.append("$indent<${renderName()}${renderAttributes()} />\n")
        }
    }

    private fun renderAttributes(): String? {
        val builder = StringBuilder()
        for (a in attributes) {
            builder.append(" ${a.first}=\"${a.second}\"")
        }
        return builder.toString()
    }

    private fun renderName(): String {
        if (xmlns == null) return name
        else return "$xmlns:$name"
    }
}